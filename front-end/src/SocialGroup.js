import React, { useState, useEffect } from "react";
import { Container, Row, Col, Figure, ListGroup } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import axios from "axios";
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import MapEmbed from "./MapEmbed";
import Spinner from "react-bootstrap/Spinner";

export default function Socialgroup() {
  const client = axios.create({
    baseURL: "https://api.flexercise.social/",
  });

  let { id } = useParams();
  const [socialgroup, setSocialgroup] = useState();
  const [gyms, setGyms] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [map, setMap] = useState();

  useEffect(() => {
    const fetchSocialgroup = async () => {
      if (socialgroup === undefined) {
        var query = `social_groupById?id=${id}`;
        await client
          .get(query)
          .then((response) => {
            setSocialgroup(response.data);
            console.log(response.data);
            setMap({
              name: response.data.name,
              addr: response.data.city + ", " + response.data.state, 
            //   width: "100%",
            //  height: "600px"
            });
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    fetchSocialgroup();
    console.log(socialgroup);
  }, [loaded]);

  useEffect(() => {
    console.log(socialgroup);
    const fetchGyms = async () => {
      var query = `gyms?page=1&perPage=20&city=` + socialgroup.city;
      await client
            .get(query)
            .then((response) => {
              setGyms(response.data);
            })
            .catch((err) => console.log(err));
    }

    fetchGyms();
  }, [socialgroup])

  console.log(id);

    return (
      <Container style={{justifyContent:'center'}} className="d-grid gap-3">
        {loaded ? (
          <div>
            <Row style={{marginTop: '25px'}}>
                <Col>
                    <h1 className="d-flex justify-content-center p-5"> {socialgroup.name} </h1>
                </Col>
            </Row>
            <Row>
                <Col style={{marginRight: '40px'}}>
                    <Figure>
                        <Figure.Image
                            src= {socialgroup.image}/>
                    </Figure> 
                </Col>
                <Col style={{marginRight: '30px'}}>
                    {!(map === undefined) && (<MapEmbed map = {map} />) }
                </Col>
                <Col>
                    <br></br>
                    <br></br>
                    <p><b>Type: </b>{socialgroup.title} </p>
                    <p><b>Address: </b>{socialgroup.address + ", " + socialgroup.city + ", " + socialgroup.state + ", " + socialgroup.country} </p>
                    <p><b>Number of Reviews: </b>{socialgroup.review_count} </p>
                    <p><b>Rating: </b>{socialgroup.rating}</p>
                    <p><b>Phone Number: </b>{socialgroup.phone} </p>
                    <br></br>

                    <Link to={`/exercise/${id}`}><Button variant="secondary" >Check out more exercise!</Button></Link>
                    &nbsp;&nbsp;
                    <h4>Check out gyms in the same area!</h4>
                    <ListGroup style={{maxHeight: "300px", overflowY: "auto"}}>
                      {gyms.map((gym) => {
                        <ListGroup.Item action href={`/gym/${gym['id']}`} style={{backgroundColor: "lavender"}}>
                            <h4>{gym['name']}</h4>
                        </ListGroup.Item>
                        return (
                          <Col>
                            <Row>
                              <Link to={`/gym/${gym['id']}`}><Button variant="secondary" >{gym['name']}</Button></Link>
                            </Row>
                          </Col>
                        );
                      })}
                    </ListGroup>
                </Col> 
            </Row>
            </div>
            ) : (<Spinner animation="grow" /> )}
       </Container>
    )
    
}
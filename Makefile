# code based off makefiles from Geo Job: https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/Makefile
# and from Sustainability: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/master/makefile

.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        


# uncomment the following line once you've pushed your test files
# you must replace GitLabID with your GitLabID

# check the existence of check files
check: $(CFILES)

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

python_unittest:
	python3 back-end/tests.py

# download latest files from the git repository
pull:
	make clean
	@echo
	git pull
	git status

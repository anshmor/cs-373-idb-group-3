import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Container, Row, Col, Figure, ListGroup } from 'react-bootstrap';
import axios from "axios";
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom'
import Spinner from "react-bootstrap/Spinner";

export default function Exercise() {
  const client = axios.create({
    baseURL: "https://api.flexercise.social/",
  });

  let { id } = useParams();
  const [exercise, setExercise] = useState();
  const [gyms, setGyms] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchExercise = async () => {
      if (exercise === undefined) {
        var query = `exerciseById?id=${id}`;
        await client
          .get(query)
          .then((response) => {
            setExercise(response.data);
            console.log(response.data);
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    fetchExercise();
    console.log(exercise);
  }, [loaded]);

  useEffect(() => {
    console.log(exercise);
    const fetchGyms = async () => {

      var query = `gyms?page=1&perPage=20&type1=gym`;
      if (exercise.equipment === 'none (bodyweight exercise)') {
        query = `gyms?page=1&perPage=10`
      }

      
      await client
            .get(query)
            .then((response) => {
              setGyms(response.data);
            })
            .catch((err) => console.log(err));
    }

    fetchGyms();
  }, [exercise])

  function formatDescription() {
    exercise.description = exercise.description.replace('</p>','');
    exercise.description = exercise.description.replace('</ p>','');
    exercise.description = exercise.description.replace('<p>','');
    exercise.description = exercise.description.replace('[','');
    exercise.description = exercise.description.replace(']','');
    return exercise.description;
  }

  console.log(id);

  const placeholder_img = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxUTEBYPExIWExYWFhYYGBYYGRYXEBYWFhcaFxYWGBYZHikhGRsmHBcXIjIiJiosLy8vGCA1OjUuOSsuLywBCgoKDg0OGxAQGy4mIR4uLi4uLi4uLi4uLi4uLC4sLi4uLiwsLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4uLv/AABEIALYBFQMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAwQCBQYHAQj/xABGEAABAwEEBggDBQQHCQAAAAABAAIDEQQSITEFBkFRcaEHExQiUmGBkRUysVNiwdHhQnKS8BcjNHOTosIkM1RjgoOys9L/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIDBAUG/8QALxEAAgEDAwIDBwQDAAAAAAAAAAECAxExBBIhQVFhcfAFEyKBkbHBMkKh0RQjM//aAAwDAQACEQMRAD8A9esuZVpVbLmVaVTKOAiLJjK4nJCx8YyqkLgMAsXSbAsFJOMGExwPBRWTapZflPBRWTaoKPJYREQsEAqjW1U7W0UkpXPgjCitg7qsKraXVBRiWCgiIqmIREQBERAS2XP0VtVLLn6K2pReOAiL6xlULBraqQuAwWLn7Ao1JOMCQ1B4KtZczwVh+R4KvZMzwUFHktIiIWC+gL41tVOxtFNglc+NYo7SO76H6KdVp31B4FGWfCNeiIqmBNZcyrSq2XMq0pLxwfW0zKOdVfEUlgiIoBhL8p4KKybVLL8p4KKybUKvJYWi05rNHA7qw0yP8I2cTit+xlV5jbgG2+XrgSOsqRtLKk4e4PqFeEU3yTLhHVaL11heS2RvUUBNXGrKDPGgVqxa52KV/VMtDbxNBeDmAncC4AErgtOSxGWsTbrK1AOwAG8fIU+oXPae0JDDBBLHO2V0jauaKVZhXZsrhjuWmxEb2e8PdVRTfKVotRLY+bR8MklS665tTmQx5aCfQBb2b5SsWrMm9ykiIqmYREQBERAS2XP0VtVLLn6K2pReOD60bSjn1XxFJYIiKAYvyPBV7JmeCsPyPBV7JmeCFXktL6xtV9Y2qmAUl0g0UQlCaKB76qSW7H176qKTI8Cs1hJkeBVSrKSIigyJrLmVaVOCuNKKfveXNSi6fBKii73lzTveXNCbkqKLveXNfO/5c0FzOX5TwWFhbUn0WT2Pukm7l51UdkvY3abM0I6o2K4bpGYysRui+b1TtutpTmV17nPG1vNcFr5I42hgdsjB93O/JVqycY3R00IqdRRa7lJ2gIpdGSTXnMeCcQcDdNA0jaDUfzgtFqzqW61Slj5rjGgF1BV5FaUGFP52rp2PI0U4bHT3fPY78FJqET18gH2f+pqpGvO6XdF56eGycrYfH8HcWOzMhjbDGLrGNDRwHmspvlKx73lzWMl6hrRanI2VkRFBmERR2hxDHFuYBook9qbfQmKu0l1JEWksTH3g8uNM88/Jbtc2k1L1EN7i4+fVdzo1On9w0r3uS2XP0VtU4K1wp6qfveXNdaMU+CVFF3vLmne8uaE3JUUXe8ua+d/y5oLkj8jwUNibVx4KQxvpU3cvNRWS9U3aZbUI6o2S+ONFXc548PNROLzu5qbl93gTOdVfGtqo2sefDzX0ufl3eaFSUupgPdRSZHgVj3vLmsXXqHLmgbKyIiqZk1lzKtKrZcyrSkvHARF9AqhYxCnYyiNbRZqxZIjn+U8CqdidSvorNpd3SPJVLGMSo6lZP4kWFx3SDZaGKauJBYRtoO8DzPuF27W0xK4vpFdUQ7qv/wBKyrfoZ0abiovXQ5rSGnYINFFkr6P66rGDF7zQVoNwBOJwwG9aXVTpGs1nlc+SKajm3atDDTvA1IveS0uvUVYWP8L6ejgfxAXPWXQUrpI43tMYkBIcaHACpwBzpsNM1Wmo2Unlfg2q71JwWHz9bH6Y0DpqG1wNtNncXRuLgCWuaatNHAhwrgQr03ylc9qHK3sbYmhreqJbRtBgTeBIGVSTxouhm+UrVPckzinHa3F9CkiIhiFXtcjrpEZAdscQXNB/dqK+6knrTBeb9JWn7RZ5YY4ZTG18bi6gaSSHUzINMKKvxSltjk6KcKah7yfKvay/P4NnqrrG99rk0baA0yxl92VguseG44t/ZdQ1w3HLb3K8P6NGufpSOQkm6JXvJqSbzHNxPm54Xt0b67KKXGMHtXr+vIrN1Ki3PC9eb8Sey5+itqpZc/RW1ZFFgIvPtaOkW7MLDo6IWy0E0NKmFlMxUfMd5qGt2nYu8sxc5jS9oa8taXNBqGuI7zQ7aAcKqS1iRTRx0X1jKLNCyRhL8p4FULE6hPBXZnYEeRVGyDE8EeSksoskqRke0rJrKYlYPdVC1rB7q4DJYoiggLCTI8Cs1hJkeBQgpIiKDMmsuZVpVbLmVbYyqk0jg+NbVTYAISAFEXY1UlsErccSsXybAsXSV8kY2qAikHdPArCwGl48PxVm0UDCPIqnZNqgq+JItOdVcj0hN7kLtznD3AP4LrVzHSFQWQSEGjJGk03Oq36kKlRNxaRrSqRhNTk7JZPNNM2HrohHWnfYa+QcK8qq66IEhxAJbW6doqKGnotdaNJ+Aev6K3ovSzRLE59G0livVqRcD2l5w+7Vcuydkbr2po3NxU1fv0fk3x+O1zvOj+zC7JKQb1Q2uNLtK8M/ousm+UpG8EAgggioIxBBxBBSb5SuuEdqsc9We+Tl3KSIik5wvNdbNJ2WbSDrBaSWxRtF1zcP65zQ41dmAGkADKta7F6UvKdO3DbJZWAd57u9tNKAmvELKrUVNXfyPS9maOWqquKdkld8Xur2tbj7m36LdGRxvtT43F7esbGxzm3XFgF6tCMiTSu27kFtdI9IdhglfBLK9j2OLXNMUtQRu7uI3HIhcrobSj4JA5uOxzdjhu47ivToaPaHllCQDRwF4YZHzCUq6q5yW9pezno5p5jLDx8uv16nKRdKujm1PXSHDIRSVPliKe64DXbpOntYMEAdBAcCAf8AaJRucW/KD4W+5yXsOl9Etns8sN0VLDTAfMMQtH0U9WY5o+ra2Zrx1jqd9wAutrwpRdCV1c8+MkuEbbUHVZliskcYja2ZzGmZ4Hec8ipaTnQVoBlgurAACYNCic6qgtgmbjisJJNgWLpF8YyqBsxcMDwKhsGBJ8lnpa0dVZ5ZfBG93G60n8F+ZDrJao3Nnjtc/Wyx3pXXzdqXu7rW5AANHDGlAlht6n6ge6q+Lzbou1itkgDLaHPZKKwTODA8mhcWODaEgtBIcRszxC9JUXTwJRafIREQgLCTI8Cs1hJkeBQgpIiKDMkgeBWqsC1jfyUNlzKtKUXjciNoC+deFMFKxm9SSkysydu0n2KkNsb/ACFM99FEShOCvLOCCooHgVqrMvyngorJtUFXe5l2gLmukZwdo+Smx0Z/zgfiurVDTti66zSwbXtIH72beYCFKsHOnKK6p/Y8EXxfV8UHyp6b0W6XLopLM8k9WQ5lcaNdgWjyBFf+pdtJMCCF5n0T17VLu6nH+JtPxXqM3ylSj6LQycqEb+P3KSIvhKg6DC0fKRWlQRUZgnaFxMep7utHfDoq945OAzpTecs9q7MkuNBkpWtoKLCUFVd+iPToaurooSjB/FPPhxx8+fVjUx6vwxyGZraHYM2NO9o2FbGzHNZTu/Z3rKNlBRSoJT4WDKpXqSo/7JOTeL8uy9cE8D6HFcHNL2HTIlGEVowO7vZ+xXe2XP0XN9JWi+tsnWtHfhN7zu7V008+ZxWdr9jqjaAvnXharU/SnaLJHJm4C6795uH5LoGM2lQ00XV2Vmzt2k+ykNsb5+yme6ihJQnlGm1st7W2G0vcaAQS82EDmvLejHQLTA20vbee5r4mtpgIy4knzLqkcB5r0zXHQ4tNn6p7yIg5rpGNwMoaRdYXbG1oTTE0GIxrBZ7O1rGhjQy6AGloAc2goKbuC5NTU/Z3z5Hdo6f738vMWCy1kYaUbG4HzFGuAAHqug7QFqdB6dbK90DiBKwkbmuANLzfPeFvFpQUVD4Xcy1iqKq1UVnxx4dGn1TyQ9oCdoCmRbHLyQ9oC+PmBBHkp1jJkeBQclFERQZmcMlFL2kbiq6ISm0WRagN/JZ9s48lTRLk7mWe0jcV87SNxVdEuRuZO+0AgihWEMl2qjRBdljtI3FZdpG4qqiXJ3M8S05HdtMrRgBI8DgHmioraa0PBtk5aajrH47K1x51WrQ+Xq/9Jeb+56D0TtobRIRsjaPdxP0C9BfOCCKFcL0YD+okO2+P/EU+pXS6b0iLPZ5JyK3G1AOFXHBo9SQiPe0fFCNvXJdJXM61Wm0QPFsifG6FrHMljkJDGVcKSimZyGJwp5lef6Q1gknf1kmJ2Y90DcBTAKpBpG4+8BsIILi5jmuBa5rmnAtIJBHmrqm8NHXCag1JP+D2mw2hr4mPaQQ5rXVBwxFVYvjePdeESyxmgZEIgBSjHOpmTU3yTXFR3m7nfxfonupdCJOLd7npFqntFhl6yWcz2TrXOrcL54xIcRI4DBja4EbtmS7CG0Ne0Pa5rmuAIIIIIORC8KhtFw3ml7SNoeQfcBTG3EuL3XnOOZc9xJPnvT3b7Fp1FO13jw6euvywke3ut0cXeke1o8yAqFo1ssT2uY6dlHAgiu/BeRfEfu8/0VZ8gJJLc/NFTkQnFdf4O86PNIiz2qWyB16OTvRkZGmRHEfRek9s48l4DZdIGN8UjRQxOqDXMVrTm73XtOjrayaJs0Zq1wrXzyI4g1CtUTyVUrcI2RtI3FfO0jcVXRZXG5nzSsxMEgYO9dN2vy1GIqua0XPNeLZWsDTkWnEHzG4rb2rSUQJgErDLT/dhzTIBtJbWoCoLg1cvjXkez7Ob9zJNLl8PrjoS2TRTO1i1VIIae6NrvlvE/uldF2kbitJBeADqGmwjGnFXopQ4ea1080ltw8nPrveTale6itvlbp65LnaRuKdpG4qui6rnnbmWO0jcUdaAQRQquiXG5hERCAiIgCIiAIiIAiIgC0muekzZ7DNO00cG3WHc95DGn0Lq+i3a5DpUiLtGPI/ZkiceF8N+rgpjlA8aitj2ila8cealOkn7mex/+lTRdDin0MZaelJ3cUX7Npu0R1Ec8kYOYY4sBpvurKXWC0uF11pmcNzpHOb7ErXIpsjVJJWR0mrlkEsbnyVPeoMThQVP15LvdGaSha1sb7PEA0BocI2uOGFXVxPGq5XVqAizMoCa3jkdpP5LYkLzataSm7PqetT01OVKO5dM4O+s9ghkaHsZA5pyIiYpfhMf2UP+ExavUkHqpN18U40x/BdItoSclc82rTUJuPY1/wAJj+yh/wAJihtWgopGOjMcQDgRVsbQ4V2gjIrbIr3ZnY4b+jeP7d/8LU/o3i/4iT+Fq7lFO+XcWOH/AKN4vt5P4Wro9XdCiyxGJsjpGl14XgBdqACBTZhVbVEcm8iwWj100ubLYZZ24PoGM8nvN0H0xPor2mNLRWaIzTvDGj+Jx8LRtK8W1y1vktzg2nVwsNWR7a0peedrqE4ZCvqphFtknOxzua/rA9wfWt+pv12m9nVdXYOkC0sbde1k1P2jVr/UtwPstDoTQ8tqmEELbzjmf2GN2ucdgXdO6JXVwtjaf3Jr/wCxXqwpz4mjSnVnTd4OxwVp0vPI4udPKakml990VNaAVoB5LGPSk7WljZ5WtrUtEjw2vAFeoWDoqgaQZp5JfJobG0/U81p+lPRcVmhs0NnibHGXSl1MS5wDQ0uccXGhdmVbdF8Ipd5Oi6KNKzT2aRsz3SdXIGtc41fdLa0LjiaHfvXbrheh6Glhkf4p3ezWMH5ruljP9TICIiqAiIgCIiAIiIAiIgCIiALk+k+03NGSjbIY2D1eHHk0rrFxPSlYHz2eJjXNbSUuINcaMIGXEqU0ndloxcnZZPGllFGXODRm4gDiTQLcnViXxs93fkt/qbqPK+0R2h7o+rila5wq684s7wAFKZhtcVsq1N4ZeVGpFXkuDds6Jottql9GsH1qrcHRXZQaulnf5XmNHJlea7tFlvl3MinZoGWeziNgIjhZRoJJN1o2naV5095cS45kknicSvQ9N/2aX+7d9F50uSu+UeloIq0md5qpDdsrT4i53Og5ALbqvYYbkTGeFjR7BWFvFWSRwVJbpuXdhERWKBUNN6S7PD1twv7wbQYCrq0JOwYK+qOmrJ1tnljpWrSR+83vN5gKs77XbJtplTdaCq/pur+V+f7PPtJ9KUkbzG2ysBG10hIPmAGjD1WltHSbbXfKYY/3WV5vcV3ep0kJjMb7l+o7rg28QBljntW4m0HY3Yus9nP/AG4/yVqNWMoJ2Ndfp/8AH1E6dmknxfjjw8PHqeFad09NaniS0SXi0UaAA1jRto0YAnaVudVdRZ7UQ9wMEPjcO84f8thz4nDivW7PoaxxuD2WeztcMiGMqPWi2XaG+ILV1OxyXKmhNCw2WIQwMujac3vPic7aVsFD2lviC+9ob4gshdEq4PpihrYon+Gdvs5jx9QF2/Xt8Q91yPSQ4SQNgwuvdeJHzAtpShyGZUb1D4pYRtp6E9RUVKnl3t8k3+Cx0YQ3dGQnxOld7yOA5ALq1wepumWQMMDy4NqLmZDaChHr/Oa7Wy2xkgJY9rgM7pBpx3Ksa0anKN9Z7PraWbjNO3ezt9SdERXOIIiIAiIgCIiAKnb9JwwU66Vkdcg4gE8BmVcXmXSlBS1RybHwgerHuB5FvurQim7MM72TTdnAr1zDwN76KlNrVCPlD38BQcyuP1b0bLaBcDboa0f1hxidsAa5tQT5eS6Bup7tsrRwaT+IXPJ1U7W9fU7Yw01k3L18kJdbz+zEPVxJ5BazTGmHWi7eaG3a5Emtab+C2LtUH7JWHiHD81iNUZftI/8AN+Szkqrz+DohLSxd45+Zzy9C0FY+qgYw4O+Z3F2NPTAei1+idWWxuEkjg8jEADug78c10C0pU3HlnPq9RGdoxwERFscRS01/Zpf7t30XA6Pivyxs8T2j0LhVd5pxwFmlqadxwHmSMl5/ZtJNs8jJnguDXZClTgd/v6LnqLdNL1k9DSS20py7X+x6eiwhlD2h7SC1wBBGRBFQVmug88IiIAsZIwc/qQskQFV9haTU578C73Iqsfhzd55K4iB85KPw8eI+yfDvvcv1V5EK7UUfh/3uX6r78P8Avcv1V1EG1FL4cPEfZZMsdMnuFVbRCdqKpsLTnjxDT9Qs4rIxpq1oB8sOQU6IW3O1rhERCAiIgCIiAIiIAuG6VLKTFDMBgx72k7usDS2vqw+67hzaihUAsrRWlRXPHAjcQc1MZWdwc7qPpOF0HVCVvWBzi4E941yNS1pdhTGmxdL17fEPdUPgFnvCQQxteDUOaxrXV9AArHw8eI8kk03dFXcn7QzxBO0s8QVf4f8Ae5L58P8Avcv1UC8ix2pniCdqZ4gq3w773L9V9+H/AHuX6oR8RY7UzxBfe0s8QVb4f97l+qfD/vcv1Qm8jnekKzyzwxtgjfLdkLnXAXFvdo0kDHaVwc+jrUfngnw3xyYeeS9gZYqYh5HDA/VSCB32r/cq8ZpdCU3br9Ty/QuuM1n7jwXtFe66ta47TiCScTiKDBq7vVjWVlsDrrHMcyl4Ghb3q0o4Z5bgtxcdtkeeJB+oWJswOdT6/kkpJ9ATogCKhIREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAf/Z"

    return (
      <Container style={{justifyContent:'center'}} className="d-grid gap-3">
      {loaded ? (
        <div>
        <Row style={{marginTop: '25px'}}>
            <Col>
                <h1 className="d-flex justify-content-center p-5"> {exercise.name} </h1>
            </Col>
        </Row>
        <Row>
            <Col style={{marginRight: '40px'}}>
                <Figure>
                    <Figure.Image
                        src= {exercise.img ? exercise.img : placeholder_img}/>
                </Figure> 
                <div className="video-section">
                  <video style ={{width:"400px",height:"400px"}} controls>
                    <source src={exercise.video} type="video/mp4"></source>
                  </video>
                </div>
            </Col>
            <Col>
                <br></br>
                <br></br>
                <p><b>Category: </b>{exercise.category} </p>
                <p><b>Muscle: </b>{exercise.muscle_name} </p>
                <p><b>Equipment: </b>{exercise.equipment} </p>
                <p><b>Description: </b>{formatDescription()}</p>
                <p><b>Front Exercise: </b>{"Front Exercise: " + exercise.is_front ? "yes" : "no"} </p>
                <br></br>
                <h4>Check out gyms you can do this exercise at!</h4>
                <ListGroup style={{maxHeight: "300px", overflowY: "auto"}}>
                      {gyms.map((gym) => {
                        <ListGroup.Item action href={`/gym/${gym['id']}`} style={{backgroundColor: "lavender"}}>
                            <h4>{gym['name']}</h4>
                        </ListGroup.Item>
                        return (
                          <Col>
                            <Row>
                              <Link to={`/gym/${gym['id']}`}><Button variant="secondary" >{gym['name']}</Button></Link>
                            </Row>
                          </Col>
                        );
                      })}
                    </ListGroup>
                &nbsp;&nbsp;
                <Link to={`/socialGroup/${id}`}><Button variant="secondary" >Check out more social groups</Button></Link>
            </Col> 
        </Row>
        </div>
        ) : (<Spinner animation="grow" /> )}
      </Container>
    )
    
}

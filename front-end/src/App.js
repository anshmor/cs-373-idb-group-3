import { BrowserRouter, Route, Routes } from 'react-router-dom';
import NavBar from './NavBar';
import Home from './Home';
import About from './About';
import GymGrid from './Gyms';
import SocialGroupGrid from './SocialGroups';
import ExerciseGrid from './Exercises';
import Gym from './Gym';
import Exercise from './Exercise';
import SocialGroup from './SocialGroup';
import {Helmet} from "react-helmet";
import './App.css';
import Search from './components/Search';
import Visualizations from './Visualizations';
import ProviderVisualizations from './ProviderVisualizations';

function App() {
  return (
    <BrowserRouter>
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Flexercise</title>
          {/* <link rel="canonical" href="http://mysite.com/example" /> */}
          <meta name="description" content="Flexercise website" />
        </Helmet>
        <NavBar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/gym" element={<GymGrid />} />
          <Route path="/socialGroups" element={<SocialGroupGrid />} />
          <Route path="/exercises" element={<ExerciseGrid />} />
          <Route path="/visualizations" element={<Visualizations />} />
          <Route path="/providerVisualizations" element={<ProviderVisualizations />} />
          <Route path="/gym/:id" element={<Gym />} />
          <Route path="/exercise/:id" element={<Exercise />} />
          <Route path="/socialGroup/:id" element={<SocialGroup />} />
          <Route path="/Search/:query" element={<Search />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

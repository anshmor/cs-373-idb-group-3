import React, { useState, useEffect, useRef } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ExerciseCard from './components/ExerciseCard';
import axios from "axios";
import Pagination from "react-bootstrap/Pagination";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FilterDropdown from "./components/FilterDropdown";
import ExerciseSort from "./components/ExerciseSort";

export default function ExerciseGrid() {
  const client = axios.create({
    baseURL: "https://api.flexercise.social/",
  });
  const searchQuery = useRef("");

  const [exercises, setExercises] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [activePage, setActivePage] = useState(1);
  const [numTotal, setNumTotal] = useState(0);

  const [category, setCategory] = useState("");
  const [equipment, setEquipment] = useState("");
  const [muscle_name, setMuscle_name] = useState("");
  const [is_front, setIs_front] = useState("");

  const[sortVal, setSortVal] = useState(null)
  //const[menuTitle, setMenuTitle] = useState("Sort by")
  const[queryR, setQueryR] = useState(null);

  var queryRE = null;
  
  function selectSortValue(val){
    console.log(val)
    setSortVal(val)
    if (val === "Alphabetical"){
    }
    if (val === "Alphabetical-Descending"){
    }
}


  function handleClick(number) {
    setActivePage(number);
    setLoaded(false);
  }

  const handleCategoryFilter = (value) => {
    console.log(value);
    setCategory(value);
  };

  const handleEquipmentFilter = (value) => {
    console.log(value);
    setEquipment(value);
  };

  const handleMuscle_nameFilter = (value) => {
    console.log(value);
    setMuscle_name(value);
  };

  const handleIs_frontFilter = (value) => {
    console.log(value);
    setIs_front(value);
  };
  // searching code inspo from https://gitlab.com/sarthaksirotiya/cs373-idb
  useEffect(() => {
    const fetchExercises = async () => {
      if (!loaded) {
        var query = `exercises?page=${activePage}&perPage=20`;
        const search = searchQuery.current.value;
        if (search) {
          query += `&search=${search}`;
          queryRE = new RegExp(`(?:${searchQuery.current.value.replaceAll(" ", "|")})`, "i");
          setQueryR(queryRE);
          console.log("queryRE: " + queryRE);
        }
        else if (sortVal) {
          query += `&sort=${sortVal}`;
          console.log(query)
        } 
        else {
          // queryRE = null;
          if (category != "") {
            query += `&category=${category}`;
            console.log("adding category filter query");
            console.log(query);
          }
          if (equipment != "") {
            query += `&equipment=${equipment}`;
            console.log("adding equipment filter query");
            console.log(query);
          }
          if (muscle_name != "") {
            query += `&muscle_name=${muscle_name}`;
            console.log("adding muscle_name filter query");
            console.log(query);
          }
          if (is_front != "") {
            query += `&is_front=${is_front}`;
            console.log("adding is_front filter query");
            console.log(query);
          }
        }
        await client
          .get(query)
          .then((response) => {
            setExercises(response.data);
            console.log(query);
            console.log(response.data);
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    const getTotalNumExercises = async () => {
      if (!loaded) {
        var query = `exerciseCount`;
        await client
          .get(query)
          .then((response) => {
            setNumTotal(response.data['count']);
            console.log(response.data);
          })
          .catch((err) => console.log(err));
      }
    };

    fetchExercises();
    getTotalNumExercises();
    console.log(exercises);
  }, [loaded]);

  useEffect(() => {
    console.log(exercises);
    console.log(numTotal);
  }, [exercises])
  
  let numPages = numTotal / 20;
  let items = [];
  for (let number = activePage - 2; number <= activePage + 2; number++) {
    if (number > 0 && number <= numPages) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={() => handleClick(number)}
          active={number === activePage}
        >
          {number}
        </Pagination.Item>
      );
    }
  }

  return (
    <Container>
      <h1 id="exercise-title" className="d-flex justify-content-center p-5">Exercises</h1>
      <Form
        onSubmit={(event) => {
          event.preventDefault();
          setLoaded(false);
        }}
        className="d-flex pb-5 justify-content-center"
      >
        <Form.Control
          id="exercise-search"
          ref={searchQuery}
          style={{ width: "20vw" }}
          type="search"
          placeholder="Search exercises"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-dark" onClick={() => setLoaded(false)}>
          Search
        </Button>
      </Form>
      <Form className="filter-form">
        <Row className="mx-auto text-center w-55 my-2"> 
        <Col className=" pb-5 justify-content-center">
        <ExerciseSort selectSortValue={selectSortValue}/>
        {/* <h3 className="test field">{menuTitle}</h3> */}
        <Button id="exercise-sort-submit" variant="outline-dark" onClick={() => setLoaded(false)} >Sort</Button>
      </Col>
          <Col>
            <FilterDropdown
              title="Category"
              items={[
                "Legs",
                "Chest",
                "Arms",
                "Shoulders"
              ]}
              onChange={handleCategoryFilter}
            />
            </Col>
            <Col>
            <FilterDropdown
              title="Equipment"
              items={[
                "Barbell",
                "Dumbbell",
                "Bench",
                "none (bodyweight exercise)"
              ]}
              onChange={handleEquipmentFilter}
            />
          </Col>
          <Col name="muscle-name-filter">
          <FilterDropdown
              title="Muscle Name"
              items={[
                "Quadriceps femoris",
                "Pectoralis major",
                "Biceps brachii",
                "Biceps femoris",
                "Triceps brachii",
                "Quadriceps femoris",
                "Trapezius",
                "Gluteus maximus",
                "Anterior deltoid"
              ]}
              onChange={handleMuscle_nameFilter}
            />
            </Col>
            <Col>
            <FilterDropdown
              title="Is a Front Exercise?"
              items={[
                "true",
                "false"
              ]}
              onChange={handleIs_frontFilter}
            />
          </Col>
        </Row>
        <Row className="mx-auto text-center mt-1">
          <Col>
            <Button
              id="exercise-filter-submit"
              variant="outline-dark"
              onClick={() => setLoaded(false)}
            >
              Filter
            </Button>
          </Col>
        </Row> 
      </Form>

      <p style={{textAlign: "center"}}>Displaying 20 out of {numTotal} exercise instances </p>
      <Row
        xl={5}
        lg={4}
        md={3}
        sm={2}
        xs={1}
        className="d-flex g-1 p-1"
        style={{marginRight: '15px', marginLeft: '15px'}}
      >
        {exercises.map((exercise) => {
            return (
              <Col className="d-flex align-self-stretch">
                <ExerciseCard exercise={exercise} regex={queryR} />
              </Col>
            );
          })}
      </Row>
      <Pagination className="justify-content-center">
        {activePage > 3 && (
          <Pagination.Item
            first
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            last
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
    </Container>
  );
}

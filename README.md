IDB Group 3

| Group Member      | Gitlab ID     | UTeid  |
| -------------     |:-------------:| ------:|
| Pinru Chen        | @becky277     | pc26863|
| Bowen Huang       | @bowenhuang1  | bh32595|
| Ansh Moraje       | @anshmor      | arm6856|
| Jeremy Tenberg    | @jrtenberg    | jrt2492|
| Prisha Singla     | @prishasingla | ps34337|

Website Name: Flexercise

Website Link: https://flexercise.social/    https://www.flexercise.social/   
              https://www.flexercise.me/       

Git SHA:
| Phase | Git SHA |
| ------| -------:|
|    1  | 25a31a82|
|    2  | 7e5520a5|
|    3  | 6cd4f73f|
|    4  | 2834bc6a|

Project leader: Organize team meetings, keep up team communication, track working progress 
| Phase | Phase Leader |         
| ------| -----------: |  
|    1  |  Pinru Chen  |
|    2  |  Ansh Moraje |
|    3  | Prisha Singla|
|    4  |  Bowen Huang |



Link to pipeline: https://gitlab.com/anshmor/cs-373-idb-group-3/-/pipelines

Estimate time phase 1:

| Group Member      | Estimate  | Actual |
| -------------     |:---------:| ------:|
| Pinru Chen        |     9     |   10   |
| Bowen Huang       |     9     |   10   |
| Ansh Moraje       |     10    |   8    |
| Jeremy Tenberg    |     12    |   11   |
| Prisha Singla     |     13    |   10   |

Estimate time phase 2:

| Group Member      | Estimate  | Actual |
| -------------     |:---------:| ------:|
| Pinru Chen        |    10     |    12  |
| Bowen Huang       |    10     |    12  |
| Ansh Moraje       |     6     |    15  |
| Jeremy Tenberg    |    10     |    11  |
| Prisha Singla     |    10     |    15  |

Estimate time phase 3:

| Group Member      | Estimate  | Actual |
| -------------     |:---------:| ------:|
| Pinru Chen        |    10     |    14  |
| Bowen Huang       |    10     |    16  |
| Ansh Moraje       |     8     |    10  |
| Jeremy Tenberg    |    10     |    11  |
| Prisha Singla     |    10     |    15  |


Estimate time phase 4:

| Group Member      | Estimate  | Actual |
| -------------     |:---------:| ------:|
| Pinru Chen        |    8      |   11   |
| Bowen Huang       |    8      |   11   |
| Ansh Moraje       |    8      |   12   |
| Jeremy Tenberg    |    9      |   10   |
| Prisha Singla     |    8      |   11   |


Comments: 
/backend/model.py and /backend/db.py
adapted from https://gitlab.com/sarthaksirotiya/cs373-idb/-/tree/main/back-end


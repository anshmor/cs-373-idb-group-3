import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import {Highlight} from "react-highlight-regex";

function OText({ children }) {
  return (
    <span style={{  fontSize: '17px', color: 'white', font: 'Courier-Oblique'  }}>{children}</span>
  );
}

const GymCard = (props) => {
  const {
    name,
    vicinity,
    icon, 
    type1,
    user_ratings_total: num_reviews,
    rating,
    operational: status,
    id, 
  } = props.gym;

  function highlightText(input) {
    
    if (props.regex != null && input != null) {
      console.log("props.regex: " + props.regex);
      console.log("run highlight funct")
      console.log("this should be highlighted");
      return <Highlight match={props.regex} text={input} />;
    }
    return input;
  }
  
  return (
    <Card
    style={{ width: '15rem', height: '30rem'}}>
    <Card.Img
      variant="top"
      src={icon}
      style={{ height: '40%', width: '100%', objectFit: 'cover' }}
      ></Card.Img>
      <Card.Body style={{height: '30%'}}>
        <Card.Title>{highlightText(name)}</Card.Title>
        <Card.Subtitle>{highlightText("Gym type: " + type1)}</Card.Subtitle>
        <Card.Text>{highlightText("Location: " + vicinity)} </Card.Text>
        <Card.Text name="rating">{highlightText("Rating: " + rating)}</Card.Text>
        <Card.Text>{highlightText("Number of Reviews: " + num_reviews ? num_reviews : 0)}</Card.Text>
        <Card.Text name="operational-text">{highlightText("Status: " + (status === 'OPERATIONAL' ? "Operational" : "Not operational"))}</Card.Text>
      </Card.Body>
      <Card.Footer
      style= {{backgroundColor: '#3d405b'}}>
        <Button
          name = "gym-instance-button"
          style= {{backgroundColor: '#e07a5f'}}
          className="btn btn-primary stretched-link"
          variant="dark"
          href={`/gym/${id}`}
        >
          <OText>More Info</OText>
        </Button>
      </Card.Footer>
    </Card>
  );
};

export default GymCard;

import Dropdown from 'react-bootstrap/Dropdown';
import { useState } from 'react';

//Dropdown Menu sorting options for social groups by highest and lowest ratings, and most and least rated.
//sortVal values match backend args for social group sorting.
//Referenced Geojobs and react-bootstrap while working on it
function SocialGroupSort({selectSortValue}) {

const[menuTitle, setMenuTitle] = useState("Sort by")

/*    Add to SocialGroups.js, now takes selectSortValue, menuTitle props
  const[sortVal, setSortVal] = useState(null)
  const[menuTitle, setMenuTitle] = useState("Sort by")

   function selectSortValue(val){
        console.log(val)
        setSortVal(val)
        if (val === "By Highest Rating"){
        setMenuTitle("Highest rating")
        }
        if (val === "By Lowest Rating"){
        setMenuTitle("Lowest rating")
        }
        if (val === "By Highest Number of Ratings"){
        setMenuTitle("Most ratings")
        }
        if (val === "By Lowest Number of Ratings"){
            setMenuTitle("Least ratings")
        }
    }
*/
  
  return (
    <div className="social-group-sort">
      <Dropdown >
        <Dropdown.Toggle variant="primary" id="dropdown-social-group-sort">
        {menuTitle}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          <Dropdown.Item id="highest-rating-social-group-sort-selection" onClick={() => {
            selectSortValue("By Highest Rating")
            setMenuTitle("Highest Rating")}}>Highest rating</Dropdown.Item>
          <Dropdown.Item id="lowest-rating-social-group-sort-selection" onClick={() => {
            selectSortValue("By Lowest Rating")
            setMenuTitle("Lowest Rating")}}>Lowest rating</Dropdown.Item>
          <Dropdown.Item onClick={() => {
            selectSortValue("By Highest Number of Ratings")
            setMenuTitle("Most Ratings")}}>Most ratings</Dropdown.Item>
          <Dropdown.Item onClick={() => {
            selectSortValue("By Lowest Number of Ratings")
            setMenuTitle("Least Ratings")
        }}>Least ratings</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
        {/*{sortVal && <h4>{sortVal}</h4>}*/}
    </div>
  );
}
export default SocialGroupSort;

/*  Submit Button style
      <button className="sort-submit" style = {{backgroundColor: '#6c757d', 
      border: 'none', color: 'white', 'border-radius': '5px'}}>Submit</button>
*/

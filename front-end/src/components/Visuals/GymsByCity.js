import React, { useState, useEffect } from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import axios from "axios";

function GymsByCity() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const client = axios.create({
      baseURL: "https://api.flexercise.social/",
    });

  useEffect(() => {
    const fetchGyms = async () => {
    
        var query = `gyms?page=1&perPage=2000`;
        var gyms;
        setLoading(true);
        await client
            .get(query)
            .then((response) => {
            gyms = response.data;
            })
            .catch((err) => console.log(err))
            .finally(() => {
              setLoading(false);
            })

        // Build Map with muscle group to frequency of exercises with that muscle group
        const gymCities = new Map();
        for (var i = 0; i < gyms.length; i++) {
            if (gymCities.has(gyms[i]["city"])) {
                gymCities.set(gyms[i]["city"], gymCities.get(gyms[i]["city"]) + 1);
            }
            else {
                gymCities.set(gyms[i]["city"], 1);
            }
        }
        
        // convert map into data for pie chart
        var tempData = [];
        gymCities.forEach((value, key, map) => {
          if (value > 20) {
            tempData.push({'city': key, 'Number of Gyms': value});
          }
        })
        
        setData(tempData);
    };

    fetchGyms();
  }, []);


  return (
    <div>
      <h1 style={{display: "flex", justifyContent: "center"}}>Number of Gyms By Popular Cities</h1>
      {loading && <h2 style={{display: "flex", justifyContent: "center"}}>Loading...</h2>}
      <BarChart width={800} height={500} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="city" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="Number of Gyms" fill="#8884d8" />
      </BarChart>
    </div>
  );
}

export default GymsByCity;

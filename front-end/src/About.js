import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Card, Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const team = [
    {
        name: "Pinru Chen",
        altName: "pinru277",
        gitlab_username: "becky277",
        responsibilities: "Front-end",
        bio:"I am a senior majoring in Computer Science and minor in Business. I like skiing, cooking, and listening to musics. ",
        tests: 5,
        email: "Beckychen0521@utexas.edu",
        image: "./teamPics/beckyHeadshot.jpg"
    },
    {
        name: "Ansh Moraje",
        altName: "ansmor",
        gitlab_username: "anshmor",
        responsibilities: "Fullstack",
        bio: "I'm a second year CS student at UT Austin. Some of my many hobbies include cars, motorocycles, rock climbing, and weightlifting.",
        tests: 10, 
        email: "ansh.moraje@gmail.com",
        image: "./teamPics/anshHeadshot.jpg"
    },
    {
        name: "Bowen Huang",
        altName: "",
        gitlab_username: "bowenhuang1",
        responsibilities: "Back-end",
        bio: "I am a second-year CS major at UT Austin from River Vale, New Jersey.  In my free time, I enjoy playing chess and working out.",
        tests: 3,
        email: "bowenhuang@utexas.edu",
        image: "./teamPics/bowenHeadshot.jpg"
    },
    {
        name: "Prisha Singla",
        altName: "",
        gitlab_username: "prishasingla",
        responsibilities: "Front-end", 
        bio: "I’m a second year CS major from Plano, Texas. In my free time, I like to dance, play guitar, and listening to music.",
        tests: 6,
        email: "prishasingla@utexas.edu",
        image: "./teamPics/prishaHeadshot.jpg"
    },
    {
        name: "Jeremy Tenberg",
        altName: "",
        gitlab_username: "jrtenberg",
        responsibilities: "Front-end",
        bio: "I am a senior Computer Science major at the University of Texas at Austin with a minor in Business. I've always loved computer technology and video games.I also enjoy traveling, cooking, bowling, and getting outside whenever I can.",
        tests: 2,
        email: "jrtenberg@gmail.com",
        image: "./teamPics/jeremyHeadshot.jpg"
    }
]

const tools = [
    {
        name: "React",
        description: "JavaScript framework for front-end development",
        image: "./toolsPic/react-logo.png",
        link: "https://reactjs.org"
    },
    {
        name: "Gitlab",
        description: "Git repository and CI/CD platform",
        image: "./toolsPic/gitlab-logo.png",
        link: "https://gitlab.com"
    },
    {
        name: "React Bootstrap",
        description: "A CSS framework that improves responsiveness",
        image: "./toolsPic/react-bootstrap-logo.png",
        link: "https://react-bootstrap.github.io/"
    },
    {
        name: "Postman",
        description: "Tool for designing and testing APIs",
        image: "./toolsPic/postman-logo.png",
        link: "https://www.postman.com/"
    },
    {
        name: "Namecheap",
        description: "Domain name register",
        image: "./toolsPic/namecheap-logo.png",
        link: "https://www.namecheap.com/"
    },
    {
        name: "Visual Studio Code",
        description: "Code editior",
        image: "./toolsPic/vsc-logo.png",
        link: "https://code.visualstudio.com/"
    },
]

const sources = [
    {
        name: "Yelp API",
        description: "API for data on social activities",
        image: "./toolsPic/yelpLogo.png",
        link: "https://www.yelp.com/"        
    },
    {
        name: "Wger API",
        description: "API for data on exercises",
        image: "./toolsPic/wgerLogo.png",
        link: "https://wger.de/en/software/features"        
    },
    {
        name: "Google Places API",
        description: "API for data on gyms",
        image: "./toolsPic/google-places-logo.png",
        link: "https://developers.google.com/maps/documentation/places/web-service"
    },

]

const names =   {'Pinru Chen': 0,
                'Ansh Moraje': 1,
                'Bowen Huang': 2, 
                'Prisha Singla': 3, 
                'Jeremy Tenberg': 4};

const accessToken = 'glpat--FtVa7NNz42zx1_imV-a';
const projectID = '43437211';

const client = axios.create({
    baseURL: 'https://gitlab.com/api/v4/projects/43437211',
    headers: {
        'Authorization': `Bearer ${accessToken}`
      }
  });

  
export default function About(){
    // NOTE some of the code to dynamically update GitLab Info inspired from:
    // Work It Out, https://gitlab.com/sriyab/workitout
    // Geo Job, https://gitlab.com/sarthaksirotiya/cs373-idb/

    const [commitsState, updateCommits] = useState([0, 0, 0, 0, 0]);
    const [issuesState, updateIssues] = useState([0, 0, 0, 0, 0]);
    const [testsState, updateTests] = useState([0, 0, 0, 0, 0]);

    useEffect(() => {
        let newCommits = [0, 0, 0, 0, 0];
        client.get("repository/contributors")
        .then((response) => {
            response.data.forEach((element) => {
                const { name, email, commits } = element;
                team.forEach((member) => {
                    if (member.name === name || member.email === email 
                        || member.gitlab_username === name || member.altName === name) {
                        newCommits[names[member.name]] += commits;
                    }
                });
            });
            updateCommits(newCommits);
        });

        let newIssues = [0, 0, 0, 0, 0];
        team.forEach((member) => {
            client.get("issues_statistics?assignee_username=" + member.gitlab_username)
            .then((response) => {
                newIssues[names[member.name]] += response.data['statistics']['counts']['closed'];
            })
        })
        updateIssues(newIssues);
      }, []);
    
    return(
        <Container>

            <h1 className="d-flex justify-content-center p-3">What is Flexercise</h1>
            <p>Flexercise helps all people find how they want to exercise. It provides them 
            information on local gyms near them, different sports/exercise organizations, 
            and specific exercises.  The website is flexible(hence the name), since it allows 
            users to pick and choose how they want to get active and stay healthy.</p>
            <h2 className="d-flex justify-content-center p-3">About our team</h2>
            
            <Row>
                {
                    team.map((member) => {
                        return (
                            <Col md="auto">
                                <Card
                                    bg={'dark'}
                                    text={'white'}
                                    style={{ width: '15rem', height: '50rem' }}>
                                    <Card.Img variant="top" style = {{ width: '15rem', height: '19rem' }} src={member.image} />
                                    <Card.Body>
                                        <Card.Title>{member.name}</Card.Title>
                                        <Card.Subtitle>{"Role: " + member.responsibilities}</Card.Subtitle>
                                        <Card.Text>{"Gitlab ID: " + member.gitlab_username}</Card.Text>
                                        <Card.Text>{"About me: " + member.bio}</Card.Text>
                                        <Card.Text>{"Commits: " + commitsState[names[member.name]]}</Card.Text>
                                        <Card.Text>{"Issues: " + issuesState[names[member.name]]}</Card.Text>
                                        <Card.Text>{"Unit Tests: " + member.tests}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })
                }
            </Row>
            <br></br>
            <br></br>
            <Container className="intro"
                style={{ 
                    backgroundColor: "#81d4fa",
                    textAlign: "center",
                    paddingTop: "15px",
                    maxWidth: "100%",
                    height: "25vh" 
                }}>

            <h2 className="d-flex justify-content-center p-4"> Total Stats</h2>
            <Row>
                <Col class="d-flex justify-content-center">
                    <h3>Total Commits: {commitsState.reduce((a, b) => a + b, 0)}</h3>
                </Col>
                <Col className="d-flex justify-content-center">
                    <h3>Total Issues: {issuesState.reduce((a, b) => a + b, 0)}</h3>
                </Col>
                <Col className="d-flex justify-content-center">
                    <h3>Total Tests: {testsState.reduce((a, b) => a + b, 0)}</h3>
                </Col>
            </Row>
            </Container>

            <h2 className="d-flex justify-content-center p-5"> Tools Used</h2>
            <Row>
                {
                    tools.map((tool) => {
                        return(
                            <Col md = "auto">
                            <Card
                                bg={'white'}
                                text={'dark'}
                                style={{ width: '16rem', margin: "1rem", height: '28rem'}}>
                                <Card.Img variant="top" src={tool.image} />
                                <Card.Body>
                                    <Card.Title>{tool.name}</Card.Title>
                                    <Card.Text>{tool.description}</Card.Text>
                                    <Link to={tool.link + ""}>{tool.link}</Link>
                                </Card.Body>
                            </Card>
                        </Col>
                        )
                    })
                }
            </Row>

            <h2 className="d-flex justify-content-center p-4"> Data Source</h2>
            <Row>
                <a className="d-flex justify-content-center" href="https://documenter.getpostman.com/view/25835044/2s93CGRG1B">Postman API Documentation Link</a>
            </Row>
        
            <Row>
                {
                    sources.map((source) => {
                        return(
                            <Col md = "auto">
                            <Card
                                bg={'white'}
                                text={'dark'}
                                style={{ width: '22rem', margin: "2rem", height: '35rem'}}>
                                <Card.Img variant="top" src={source.image} />
                                <Card.Body>
                                    <Card.Title>{source.name}</Card.Title>
                                    <Card.Text>{source.description}</Card.Text>
                                    <Link to={source.link + ""}>{source.link}</Link>
                                </Card.Body>
                            </Card>
                        </Col>
                        )
                    })
                }
            </Row>

            
        </Container>
    );
}
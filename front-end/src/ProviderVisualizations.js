import RestaurantByReviews from './components/Visuals/RestaurantByReviews';
import HotelsByCity from './components/Visuals/HotelsByCity';
import AttractionsByCategories from './components/Visuals/AttractionsByCategories';

const ProviderVisualizations = () => {
    return ( 
        <div 
        style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "100%"}}>
            <div>
                <HotelsByCity />
                <AttractionsByCategories />
                <RestaurantByReviews />
            </div>
    
        </div>        
     );
}
 
export default ProviderVisualizations;
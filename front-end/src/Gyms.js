import React, { useState, useEffect, useRef } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import GymCard from './components/GymCard';
import axios from "axios";
import Pagination from "react-bootstrap/Pagination";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FilterDropdown from "./components/FilterDropdown";
import GymSort from "./components/GymSort";

export default function GymGrid() {
  const client = axios.create({
    baseURL: "https://api.flexercise.social/",
  });

  const searchQuery = useRef("");

  const [gyms, setGyms] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [activePage, setActivePage] = useState(1);
  const [numTotal, setNumTotal] = useState(0);
  
  const [sort, setSort] = useState("sort");
  const [ascending, setAscending] = useState(false);
  
  const [startRating, setStartRating] = useState(0.0);
  const [endRating, setEndRating] = useState(5.0);
  const [minNumRatings, setMinNumRatings] = useState(0);
  const [maxNumRatings, setMaxNumRatings] = useState(1000);
  const [operational, setOperational] = useState("");
  const [typeOfGym, setTypeOfGym] = useState("");

  const[sortVal, setSortVal] = useState(null)
  const[menuTitle, setMenuTitle] = useState("Sort by")

  const[queryR, setQueryR] = useState(null);

  var queryRE = null;

  function selectSortValue(val){
        console.log(val)
        setSortVal(val)
        if (val === "By Highest Rating"){
        setMenuTitle("Highest rating")
        }
        if (val === "By Lowest Rating"){
        setMenuTitle("Lowest rating")
        }
        if (val === "By Highest Number of Ratings"){
        setMenuTitle("Most ratings")
        }
        if (val === "By Lowest Number of Ratings"){
            setMenuTitle("Least ratings")
        }
    }


  const handleSortFilter = (value) => {
    setSort(value.toLowerCase().replace(" ", "_"));
  };
  const handleOrderFilter = (value) => {
    setAscending(value == "Ascending");
  };

  const handleOperationalFilter = (value) => {
    console.log(value);
    setOperational(value);
  };

  const handleTypeGymFilter = (value) => {
    console.log(value);
    setTypeOfGym(value);
  };

  function updateNumConstraints(category) {
    var val = document.getElementById(category).value;
    console.log(val);

    if (category === 'startRating') {
      if (val !== '0' && !isNaN(val)) {
        setStartRating(val);
      } else {
        setStartRating(0.0);
      }
    } else if (category === 'endRating') {
      if (val !== '0' && !isNaN(val)) {
        setEndRating(val);
      } else {
        setEndRating(5.0);
      }
    } else if (category === 'minNumRating') {
      if (val !== '0' && !isNaN(val)) {
        setMinNumRatings(val);
      } else {
        setMinNumRatings(0);
      }
    } else if (category === 'maxNumRating') {
      if (val !== '0' && !isNaN(val)) {
        setMaxNumRatings(val);
      } else {
        setMaxNumRatings(1000);
      }
    }
  }


  function handleClick(number) {
    setActivePage(number);
    setLoaded(false);
  }
  // searching code inspo from https://gitlab.com/sarthaksirotiya/cs373-idb
  useEffect(() => {
    const fetchGyms = async () => {
      if (!loaded) {
        var query = `gyms?page=${activePage}&perPage=20`;
        const search = searchQuery.current.value;
        if (search) {
          query += `&search=${search}`;
          queryRE = new RegExp(
            `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
            "i"
          );
          setQueryR(queryRE);
          console.log("queryRE: " + queryRE);
        }
        else if (sortVal) {
          query += `&sort=${sortVal}`;
          console.log(query)
        }
        else {
          if (startRating != 0 || endRating != 5) {
            query += `&rating=${startRating},${endRating}`;
            console.log("adding rating filter query");
            console.log(query);
          }
          if (minNumRatings != 0 || maxNumRatings != 1000) {
            query += `&numberOfRatings=${minNumRatings},${maxNumRatings}`;
            console.log("adding num rating filter query");
            console.log(query);
          }
          if (operational != "") {
            query += `&operational=${operational}`;
            console.log("adding operational filter query");
            console.log(query);
          }
          if (typeOfGym != "") {
            query += `&typeOfGym=${typeOfGym}`;
            console.log("adding typeOfGym filter query");
            console.log(query);
          }

        }
        await client
          .get(query)
          .then((response) => {
            setGyms(response.data);
            console.log(query);
            console.log(response.data);
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    }
    
    const getTotalNumGyms = async () => {
      if (!loaded) {
        var query = `gymCount`;
        await client
          .get(query)
          .then((response) => {
            setNumTotal(response.data['count']);
            console.log(response.data);
          })
          .catch((err) => console.log(err));
      }
    };
    fetchGyms();
    getTotalNumGyms();
    console.log(gyms)
  }, [loaded]);

  useEffect(() => {
    console.log(gyms);
    console.log(numTotal)
  }, [gyms])

  let numPages = Math.ceil(numTotal / 20);
  let items = [];
  for (let number = activePage - 2; number <= activePage + 2; number++) {
    if (number > 0 && number <= numPages) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={() => handleClick(number)}
          active={number === activePage}
        >
          {number}
        </Pagination.Item>
      );
    }
  }

  return (
    <Container>
      <h1 id="gym-title" className="d-flex justify-content-center p-5">Gyms</h1>
      <Form
        onSubmit={(event) => {
          event.preventDefault();
          setLoaded(false);
        }}
        className="d-flex pb-5 justify-content-center"
      >
        <Form.Control
          id="gym-search"
          ref={searchQuery}
          style={{ width: "20vw" }}
          type="search"
          placeholder="Search gyms"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-dark" onClick={() => setLoaded(false)}>
          Search
        </Button>
      </Form>
      <Form className="filter-form">
  <Row className="mx-auto text-center w-50 my-4">
  <Col className=" pb-5 justify-content-center">
        <GymSort menutitle={menuTitle} selectSortValue={selectSortValue}/>
        {/* <h7 className="test field">{menuTitle}</h7> */}
        <Button id="gym-sort-submit" variant="outline-dark" onClick={() => setLoaded(false)} >Sort</Button>
      </Col>
    <Col>
      <FilterDropdown
        title="Sort"
        items={["Sort", "Rating"]}
        onChange={handleSortFilter}
      />
    </Col>
    <Col>
      <FilterDropdown
        title="Order"
        items={["Ascending", "Descending"]}
        onChange={handleOrderFilter}
      />
    </Col>
    <Col name="gym-operational-filter">
      <FilterDropdown
        title="Operational"
        items={[
          "OPERATIONAL",
          "CLOSED_TEMPORARILY",
        ]}
        onChange={handleOperationalFilter}
      />
    </Col>
    <Col>
      <FilterDropdown
        title="Gym Type"
        items={[
          "gym",
          "lodging",
          "restaurant",
          "point_of_interest",
          "physiotherapist",
          "spa",
          "stadium",
          "school"
        ]}
        onChange={handleTypeGymFilter}
      />
    </Col>
    </Row>
    <Row>
    <Col>
      <div class = "form-group">
        <label for = "formGroupExampleInput">Min Rating(0.0 - 5.0)</label>
        <input 
          type="number"
          class="form-control"
          id="startRating"
          placeholder="0"
          onChange={() => updateNumConstraints("startRating")}
        ></input>
      </div>
    </Col>
    <Col>
      <div class = "form-group">
        <label for = "formGroupExampleInput">Max Rating(0.0 - 5.0)</label>
        <input 
        type="number"
        class="form-control"
        id="endRating"
        placeholder="5"
        onChange={() => updateNumConstraints("endRating")}
      ></input>
      </div>
    </Col>
    <Col>
      <div class = "form-group">
        <label for = "formGroupExampleInput">Min Num Ratings</label>
        <input 
          type="number"
          class="form-control"
          id="minNumRating"
          placeholder="0"
          onChange={() => updateNumConstraints("minNumRating")}
        ></input>
      </div>
    </Col>
    <Col>
      <div class = "form-group">
        <label for = "formGroupExampleInput">Max Num Ratings</label>
        <input 
        type="number"
        class="form-control"
        id="maxNumRating"
        placeholder="1000"
        onChange={() => updateNumConstraints("maxNumRating")}
      ></input>
      </div>
    </Col>
  </Row>
  <Row className="mx-auto text-center mt-4">
    <Col>
      <Button
        id="gym-filter-submit"
        variant="outline-secondary"
        onClick={() => setLoaded(false)}
      >
        Filter
      </Button>
    </Col>
  </Row> 
</Form>


      <p style={{textAlign: "center"}}>Displaying 20 out of {numTotal} gym instances </p>
      <Row
        xl={5}
        lg={4}
        md={3}
        sm={2}
        xs={1}
        className="d-flex g-1 p-1"
        style={{marginRight: '15px', marginLeft: '15px'}}
      >
        {gyms.map((gym) => {
            return (
              <Col className="d-flex align-self-stretch">
                <GymCard gym={gym} regex={queryR}/>
              </Col>
            );
          })}
      </Row>
      <Pagination className="justify-content-center">
        {activePage > 3 && (
          <Pagination.Item
            first
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
          >
            1
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            last
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
    </Container>
  );
}
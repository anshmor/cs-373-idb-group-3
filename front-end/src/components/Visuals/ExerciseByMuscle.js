import React, { useState, useEffect } from "react";
import { PieChart, Pie, Legend, Tooltip, ResponsiveContainer, Cell, Label } from 'recharts';
import axios from "axios";

const ExerciseByMuscle = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);

    const COLORS = ["#CB3503","#E65D0A","#FB931F","#372B4F", "#8070c0"]

    const client = axios.create({
        baseURL: "https://api.flexercise.social/",
      });

    useEffect(() => {
        const fetchExercises = async () => {
        
            var query = `exercises`;
            var exercises;
            setLoading(true);
            await client
                .get(query)
                .then((response) => {
                exercises = response.data;
                })
                .catch((err) => console.log(err))
                .finally(() => {
                    setLoading(false);
                })

            // Build Map with muscle group to frequency of exercises with that muscle group
            const exerciseMuscles = new Map();
            for (var i = 0; i < exercises.length; i++) {
                if (exerciseMuscles.has(exercises[i]["category"])) {
                    exerciseMuscles.set(exercises[i]["category"], exerciseMuscles.get(exercises[i]["category"]) + 1);
                }
                else {
                    exerciseMuscles.set(exercises[i]["category"], 1);
                }
            }
            
            // convert map into data for pie chart
            var tempData = [];
            exerciseMuscles.forEach((value, key, map) => {
                tempData.push({'name': key, 'value': value});
            })
            
            setData(tempData);
        };

    fetchExercises();
    }, []);

    return ( 
    <div>
        <h1 style={{display: "flex", justifyContent: "center"}}>Exercises by Muscle Group</h1>
        {loading && <h2 style={{display: "flex", justifyContent: "center"}}>Loading...</h2>}
        <PieChart   width={730} height={500}>
            <Pie data={data} dataKey="value" nameKey="name" 
              cx="50%" cy="50%" fill= "#FFC300" label>
                {
          	data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
          }
            </Pie>
               <Tooltip />
        </PieChart>
    </div>
        );
}
 
export default ExerciseByMuscle;
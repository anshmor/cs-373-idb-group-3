import React, { useState, useEffect } from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import axios from "axios";

function HotelsByCity() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const client = axios.create({
      baseURL: "https://api.cityscoop.me/",
    });

  useEffect(() => {
    const fetchHotels = async () => {
    
        var query = `hotels`;
        var hotels;
        setLoading(true);
        await client
            .get(query)
            .then((response) => {
                hotels = response.data['Result'];
                
            })
            .catch((err) => console.log(err))
            .finally(() => {
              setLoading(false);
            })

        // Build Map with muscle group to frequency of exercises with that muscle group
        const hotelCities = new Map();
        for (var i = 0; i < hotels.length; i++) {
            let city = hotels[i]["hot_city"].toUpperCase();
            if (hotelCities.has(city)) {
                hotelCities.set(city, hotelCities.get(city) + 1);
            }
            else {
                hotelCities.set(city, 1);
            }
        }
        
        // convert map into data for pie chart
        var tempData = [];
        hotelCities.forEach((value, key, map) => {
            tempData.push({'city': key, 'Number of Hotels': value});
        })
        
        setData(tempData);
    };

    fetchHotels();
  }, []);


  return (
    <div>
      <h1 style={{display: "flex", justifyContent: "center"}}>Number of Hotels By Popular Cities</h1>
      {loading && <h2 style={{display: "flex", justifyContent: "center"}}>Loading...</h2>}
      <BarChart width={800} height={500} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="city" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="Number of Hotels" fill="#8884d8" />
      </BarChart>
    </div>
  );
}

export default HotelsByCity;

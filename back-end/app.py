from flask import request, jsonify
from sqlalchemy import or_
from models import app, Gym, Exercise, Social_Group, City
from jsonschema import Draft7Validator
import json

with open("schemas.json", "r") as f:
    schemas = json.load(f)

def validate_schema(schema, data):
    for i in data.json:
        errors = validate_json(i, schema)
        if errors:
            print(list(errors))
            return False
    return True

def validate_json(json_data, schema_name):
    schema = schemas[schema_name]
    Draft7Validator.check_schema(schema)
    validator = Draft7Validator(schema)
    errors = validator.iter_errors(json_data)
    return list(errors)


# default route for testing
@app.route("/")
def hello():
    return "Hello World!"

@app.route("/search/<s>")
def searchAll(s):
    page = request.args.get("page")
    perPage = request.args.get("perPage")

    exercises = Exercise.query
    gyms = Gym.query
    social_groups = Social_Group.query

    #search through all three models
    keywords = str(s)
    exercises = searchQuery(exercises, keywords.title(), "exercises").all()
    gyms = searchQuery(gyms, keywords.title(), "gyms").all()
    social_groups = searchQuery(social_groups, keywords.title(), "social_groups").all()
    exercise_result = [exercise.to_dict() for exercise in exercises]
    gym_result = [gym.to_dict() for gym in gyms]
    social_group_result = [social_group.to_dict() for social_group in social_groups]

    #validate model schemas
    if not (validate_schema("exercise", jsonify(exercise_result)) and
        validate_schema("gym", jsonify(gym_result)) and 
        validate_schema("social_group", jsonify(social_group_result))):
        return "error"

    #combine and paginate(if needed)
    # result = exercise_result + gym_result + social_group_result
    result = {}
    result['gyms'] = gym_result
    result['exercises'] = exercise_result
    result['social_groups'] = social_group_result
    
    # if not (page == None and perPage == None):
    #     start_index = (int(page)-1) * int(perPage)
    #     #paginate combined list
    #     result = result[start_index : start_index+int(perPage)]
    return jsonify(result)

# return list of objects that have keywords in search
# search is string of keywords with space in between
def searchQuery(query, search, type):
    q = query
    search_list = search.split(",")
    
    for searches in search_list:
        if type == "exercises":
            q = q.filter(or_(Exercise.name.like('%' + searches + '%'),
                            Exercise.category.like('%' + searches + '%'),
                            Exercise.equipment.like('%' + searches + '%'),
                            Exercise.muscle_name.like('%' + searches + '%'),
                            Exercise.description.like('%' + searches + '%'),
                            ))
        elif type == "gyms":
            city_obj = City.query.filter(City.city == searches).first()

            q = q.filter(or_(Gym.name.like('%' + searches + '%'),
                            Gym.vicinity.like('%' + searches + '%'),
                            False if city_obj == None else Gym.city_id == city_obj.id,
                            Gym.type1.like('%' + searches + '%'),
                            Gym.type2.like('%' + searches + '%'),
                            Gym.type3.like('%' + searches + '%'),
                            ))
        else:
            city_obj = City.query.filter(City.city == searches).first()

            q = q.filter(or_(Social_Group.name.like('%' + searches + '%'),
                            Social_Group.address.like('%' + searches + '%'),
                            Social_Group.country.like('%' + searches + '%'),
                            Social_Group.state.like('%' + searches + '%'),
                            False if city_obj == None else Social_Group.city_id == city_obj.id,
                            Social_Group.zip_code.like('%' + searches + '%'),
                            Social_Group.title.like('%' + searches + '%'),
                            Social_Group.phone.like('%' + searches + '%'),
                            ))

    return q

@app.route("/exerciseCount")
def exerciseCount():
    return {'count': Exercise.query.count()}

# get exercise by id, empty dict if id not specified
@app.route("/exerciseById")
def exerciseById():
    id = request.args.get("id")
    if id == None:
        return {}

    exercise = Exercise.query.filter(Exercise.id == id).first()
    data = jsonify(exercise.to_dict())

    errors = validate_json(data.json, "exercise")
    if errors:
        print(list(errors))
        return "error"

    return data


# get all exercises, unless page and perPage is specified
@app.route("/exercises")
def exercises():
    page = request.args.get("page")
    perPage = request.args.get("perPage")
    category = request.args.get("category")
    equipment = request.args.get("equipment")
    muscle_name = request.args.get("muscle_name")
    is_front = request.args.get("is_front")
    sort = request.args.get("sort")
    search = request.args.get("search")
    data = None

    if not page or not perPage :
        exercises = Exercise.query.all()
        data = jsonify([exercise.to_dict() for exercise in exercises])

    else:
        exercises = Exercise.query
        if category :
            exercises = exercises.filter(Exercise.category == category)
        
        if equipment :
            if equipment == 'notNone' :
                exercises = exercises.filter(Exercise.equipment != 'none (bodyweight exercise)')

            else :
                exercises = exercises.filter(Exercise.equipment == equipment)
        
        if muscle_name :
            exercises = exercises.filter(Exercise.muscle_name == muscle_name)

        if is_front :
            exercises = exercises.filter(Exercise.is_front == is_front)
        
        if search :
            exercises = searchQuery(exercises, search, "exercises")
            
        if sort :
            if sort == "Alphabetical":
                exercises = exercises.order_by(Exercise.name)
            else: 
                exercises = exercises.order_by(Exercise.name.desc())
        else:
            exercises = exercises.order_by(Exercise.id)


        exercises = exercises.all()
        start_index = (int(page)-1) * int(perPage)
        #paginate with list slicing
        data = jsonify([exercise.to_dict() for exercise in exercises[start_index : start_index+int(perPage)]])

    # validate json to return with schema
    if not validate_schema("exercise", data):
        return "error"
    return data

@app.route("/gymCount")
def gymCount():
    return {'count': Gym.query.count()}

# get gym by id, empty dict if id not specified
@app.route("/gymById")
def gymById():
    id = request.args.get("id")
    if id == None:
        return {}

    gym = Gym.query.filter(Gym.id == id).first()
    data = jsonify(gym.to_dict())
    errors = validate_json(data.json, "gym")
    if errors:
        print(list(errors))
        return "error"

    return data


# get all gyms, unless page and perPage are specified
@app.route("/gyms")
def gyms():
    page = request.args.get("page")
    perPage = request.args.get("perPage")
    rating = request.args.get("rating")
    numberOfRatings = request.args.get("numberOfRatings")
    operational = request.args.get("operational")
    typeOfGym = request.args.get("typeOfGym")
    sort = request.args.get("sort")
    search = request.args.get("search")
    city = request.args.get("city")
    data = None

    if not page or not perPage:
        gyms = Gym.query.all()
        data = jsonify([gym.to_dict() for gym in gyms])

    else:
        gyms = Gym.query
        if rating :
            gyms = gyms.filter(float(rating[:3]) <= Gym.rating, 
                               Gym.rating <= float(rating[4:]))

        if numberOfRatings :
            numRatingRange = numberOfRatings.split(",")
            gyms = gyms.filter(int(numRatingRange[0]) <= Gym.numberOfRatings, 
                               Gym.numberOfRatings <= int(numRatingRange[1]))
        
        if operational :
            gyms = gyms.filter(Gym.operational == operational)

        if typeOfGym :
            gyms = gyms.filter(or_(typeOfGym == Gym.type1,
                               typeOfGym == Gym.type2,
                               typeOfGym == Gym.type3))
            
        if city :
            city = city.title()
            city_obj = City.query.filter(City.city == city).first()
            
            if city_obj :
                gyms = gyms.filter(Gym.city_id == city_obj.id)

        if not search == None:
            gyms = searchQuery(gyms, search, "gyms")

        if sort :
            if sort == "By Highest Rating":
                gyms = gyms.order_by(Gym.rating.desc())
            elif sort == "By Lowest Rating":
                gyms = gyms.order_by(Gym.rating)
            elif sort == "By Highest Number of Ratings":
                gyms = gyms.order_by(Gym.numberOfRatings.desc())
            else:
                gyms = gyms.order_by(Gym.numberOfRatings)
            
        else:
            gyms = gyms.order_by(Gym.id)

        # another way to paginate using query.paginate()
        # gyms = gyms.paginate(page=int(page), per_page=int(perPage), error_out=False).items

        gyms = gyms.all()

        start_index = (int(page)-1) * int(perPage)
        #paginate with list slicing
        data = jsonify([gym.to_dict() for gym in gyms[start_index : start_index+int(perPage)]])

    # validate json to return with schema
    if not validate_schema("gym", data):
        return "error"
    return data


@app.route("/social_groupCount")
def social_groupCount():
    return {'count': Social_Group.query.count()}

# get social_group by id, empty dict if id not specified
@app.route("/social_groupById")
def social_groupById():
    id = request.args.get("id")
    if id == None:
        return {}

    social_group = Social_Group.query.filter(Social_Group.id == id).first()
    data = jsonify(social_group.to_dict())
    errors = validate_json(data.json, "social_group")

    if errors:
        print(list(errors))
        return "error"

    return data


# get all social_groups, unless page and per page are provided
@app.route("/social_groups")
def social_groups():
    page = request.args.get("page")
    perPage = request.args.get("perPage")
    rating = request.args.get("rating")
    numberOfRatings = request.args.get("numberOfRatings")
    city = request.args.get("city")
    country = request.args.get("country")
    state = request.args.get("state")
    title = request.args.get("title")
    sort = request.args.get("sort")
    search = request.args.get("search")
    data = None

    if not page or not perPage :
        social_groups = Social_Group.query.all()
        data = jsonify([social_group.to_dict() for social_group in social_groups])

    else:
        social_groups = Social_Group.query
        if rating :
            social_groups = social_groups.filter(float(rating[:3]) <= Social_Group.rating, 
                               Social_Group.rating <= float(rating[4:]))

        if numberOfRatings :
            numRatingRange = numberOfRatings.split(",")
            social_groups = social_groups.filter(int(numRatingRange[0]) <= Social_Group.numberOfRatings, 
                               Social_Group.numberOfRatings <= int(numRatingRange[1]))
        if country :
            country = country.upper()
            social_groups = social_groups.filter(Social_Group.country == country)
        
        if state :
            state = state.upper()
            social_groups = social_groups.filter(Social_Group.state == state)
        
        if city :
            city = city.title()
            city_obj = City.query.filter(City.city == city).first()
            if city_obj :
                social_groups = social_groups.filter(Social_Group.city_id == city_obj.id)

        if title :
            title = title.title()
            social_groups = social_groups.filter(Social_Group.title == title)

        if not search == None:
            social_groups = searchQuery(social_groups, search, "social_groups")

        if sort :
            if sort == "By Highest Rating":
                social_groups = social_groups.order_by(Social_Group.rating.desc())
            elif sort == "By Lowest Rating":
                social_groups = social_groups.order_by(Social_Group.rating)
            elif sort == "By Highest Number of Ratings":
                social_groups = social_groups.order_by(Social_Group.numberOfRatings.desc())
            else:
                social_groups = social_groups.order_by(Social_Group.numberOfRatings)
        else:
            social_groups = social_groups.order_by(Social_Group.id)

        social_groups = social_groups.all()

        start_index = (int(page)-1) * int(perPage)
        #paginate with list slicing
        data = jsonify([social_group.to_dict() for social_group in social_groups[start_index : start_index+int(perPage)]])

    # validate json to return with schema
    if not validate_schema("social_group", data):
        return "error"
    return data



# run app, not going to be used in production
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
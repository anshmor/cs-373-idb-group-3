import React from "react";

//embedId is the videoId value from Youtube API search.
//'rKpcpsvclw0' warmup video placeholder.
let embedId = 'rKpcpsvclw0'

//Search using the instance exercise name and embed most relevant result.
//Shows embedded youtube video with given Id.
const YoutubeEmbed = () => ( 

  <div className="video-responsive">
     <h1> Demonstration Video</h1>
    <iframe
      width="597"
      height="336"
      src={`https://www.youtube.com/embed/${embedId}`}
      allowFullScreen
    />
  </div>
);

export default YoutubeEmbed;


//Issue: Couldn't parse youtube fetch result to get id
//Was able to see it in console logs, but not get it into a variable.
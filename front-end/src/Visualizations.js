import ExerciseByMuscle from "./components/Visuals/ExerciseByMuscle";
import GymsByCity from "./components/Visuals/GymsByCity";
import SocialGroupActivities from "./components/Visuals/SocialGroupActivities";

const Visualizations = () => {
    return ( 
        <div 
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "100%"}}>
          <div >
            <ExerciseByMuscle/>
            <GymsByCity/>
            <SocialGroupActivities />
          </div>
    
        </div>        
     );
}
 
export default Visualizations;
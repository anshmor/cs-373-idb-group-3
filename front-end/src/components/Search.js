import { Col, Container, Row, Spinner, Tab, Tabs } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import SocialGroupCard from "../components/SocialGroupCard";
import GymCard from "../components/GymCard";
import ExerciseCard from "../components/ExerciseCard";
import axios from "axios";
import { useEffect, useState } from "react";

const Search = () => {
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false);

  const location = useLocation();
  const userQuery = location.pathname.split("/search/").at(-1);
  //const queryRE = new RegExp(`(?:${userQuery.replaceAll("%20", "|")})`, "i");
  const[queryR, setQueryR] = useState(null);

  var queryRE = null;

  // setQueryR(userQuery.replaceAll("%20", ","));

  
  console.log(queryR);
  const client = axios.create({
    baseURL: "https://api.flexercise.social/",
  });


  const getResults = async () => {
    if (!loaded) {
      setQueryR(new RegExp(`(?:${userQuery.replaceAll("%20", "|")})`, "i"));

      await client
      .get('search/' + userQuery)
      .then((response) => {
        setData(response.data);
        setLoaded(true);
        console.log(userQuery);
        console.log(queryR);
        console.log(response.data);
      })
      .catch((err) => console.log(err));
    }
  };

  useEffect(() => {
    getResults();
  }, []);

  return (
    <Container>
      <h1>Search Results</h1>
      <Tabs defaultActiveKey="Gyms">
        <Tab eventKey="Gyms" title="Gyms">
          <Row xl={4} lg={3} md={2} sm={1} xs={1}>
            {loaded ? (
              data["gyms"].map((gym) => (
                <Col key={gym.id}>
                  <GymCard gym={gym} regex={queryR} />
                </Col>
              ))
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Tab>
        <Tab eventKey="Exercises" title="Exercises">
          <Row xl={4} lg={3} md={2} sm={1} xs={1}>
            {loaded ? (
              data["exercises"].map((exercise) => (
                <Col key={exercise.id}>
                  <ExerciseCard exercise={exercise} regex={queryR} />
                </Col>
              ))
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Tab>
        <Tab eventKey="SocialGroups" title="Social Groups">
          <Row xl={4} lg={3} md={2} sm={1} xs={1}>
            {loaded ? (
              data["social_groups"].map((socialgroup) => (
                <Col key={socialgroup.id}>
                  <SocialGroupCard socialgroup={socialgroup} regex={queryR} />
                </Col>
              ))
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Tab>
      </Tabs>
    </Container>
  );
};

export default Search;

import React, { useState, useEffect } from "react";
import { Container, Row, Col, Figure, ListGroup } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import axios from "axios";
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom'
import Spinner from "react-bootstrap/Spinner";
import MapEmbed from "./MapEmbed";

export default function Gym() {
  const client = axios.create({
    baseURL: "https://api.flexercise.social/",
  });

  let { id } = useParams();
  const [gym, setGym] = useState();
  const [social_groups, setSocial_groups] = useState([]);
  const [exercises, setExercises] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [map, setMap] = useState();

  useEffect(() => {
    const fetchGym = async () => {
      if (gym === undefined) {
        var query = `gymById?id=${id}`;
        await client
          .get(query)
          .then((response) => {
            setGym(response.data);
            console.log(response.data);
            setMap({
              name: response.data.name,
              addr: response.data.vicinity 
            });
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    fetchGym();
    console.log(gym);
  }, [loaded]);

  useEffect(() => {
    console.log(gym);
    const fetchSocialGroups = async () => {
      var query = `social_groups?page=1&perPage=20&city=` + gym.city;
      await client
            .get(query)
            .then((response) => {
              setSocial_groups(response.data);
            })
            .catch((err) => console.log(err));
    }

    const fetchExercises = async () => {
      let equipment = 'notNone';

      if (gym.type1 !== 'gym') {
        equipment = 'none%20(bodyweight%20exercise)'
      }


      var query = `exercises?page=1&perPage=20&equipment=` + equipment;
      await client
            .get(query)
            .then((response) => {
              setExercises(response.data);
            })
            .catch((err) => console.log(err));
    }

    fetchExercises();
    fetchSocialGroups();
    
  }, [gym])


  console.log(id);

    return (
      <Container style={{justifyContent:'center'}} className="d-grid gap-3">
        {loaded ? (
          <div>
            <Row style={{marginTop: '25px'}}>
                <Col>
                    <h1 className="d-flex justify-content-center p-5"> {gym.name} </h1>
                </Col>
            </Row>
            <Row>
                <Col style={{marginRight: '40px'}}>
                    <Figure>
                        <Figure.Image
                            src= {gym.icon}/>
                    </Figure> 
                </Col>
                <Col style={{marginRight: '40px'}}>
                    {!(map === undefined) && (<MapEmbed map = {map} />) }
                </Col>
                <Col>
                    <br></br>
                    <br></br>
                    <p><b>Gym Type: </b>{gym.type1} </p>
                    <p><b>Location: </b>{gym.vicinity} </p>
                    <p><b>Number of Reviews: </b>{gym.user_ratings_total} </p>
                    <p><b>Rating: </b>{gym.rating}</p>
                    <p><b>Status: </b>{gym.status === 'OPERATIONAL' ? "Operational" : "Not operational"} </p>
                    <br></br>
                    <h4>Check out exercises you could do at this gym!</h4>
                    <ListGroup style={{maxHeight: "300px", overflowY: "auto"}}>
                      {exercises.map((exercise) => {
                        <ListGroup.Item action href={`/socialGroup/${exercise['id']}`} style={{backgroundColor: "lavender"}}>
                            <h4>{exercise['name']}</h4>
                        </ListGroup.Item>
                        return (
                          <Col>
                            <Row>
                              <Link to={`/exercise/${exercise['id']}`}><Button variant="secondary" >{exercise['name']}</Button></Link>
                            </Row>
                          </Col>
                        );
                      })}
                    </ListGroup>
                    &nbsp;&nbsp;
                    <h4>Check out social groups in the same area!</h4>
                    <ListGroup style={{maxHeight: "300px", overflowY: "auto"}}>
                      {social_groups.map((social_group) => {
                        <ListGroup.Item action href={`/socialGroup/${social_group['id']}`} style={{backgroundColor: "lavender"}}>
                            <h4>{social_group['name']}</h4>
                        </ListGroup.Item>
                        return (
                          <Col>
                            <Row>
                              <Link to={`/socialGroup/${social_group['id']}`}><Button variant="secondary" >{social_group['name']}</Button></Link>
                            </Row>
                          </Col>
                        );
                      })}
                    </ListGroup>
                </Col> 
            </Row>
            </div>
            ) : (<Spinner animation="grow" /> )}
            </Container>
    )
    
}

import React from "react";
import { render } from '@testing-library/react';
import NavBar from "../NavBar";
import About from "../About";
import Exercise from "../Exercises";
import Gym from "../Gyms";
import Socialgroup from "../SocialGroup";
import App from "../App";
import GymCard from "../components/GymCard";
import ExerciseCard from "../components/ExerciseCard";
import SocialGroupCard from "../components/SocialGroupCard";

// Test 1
  test("Render about page", () => {
    render(<About/>);
    const titleElem = screen.getByText("About our team");
    expect(titleElem).toBeInTheDocument();
  })

// Test 2
  test("Render Exercise page", () => {
    render(<Exercise/>);
    const titleElem = screen.getByText("Exercises");
    expect(titleElem).toBeInTheDocument();
  })

// Test 3
  test("Render Gym page", () => {
    render(<Gym/>);
    const titleElem = screen.getByText("Gyms");
    expect(titleElem).toBeInTheDocument();
  })

// Test 4
  test("Render SocialGroup page", () => {
    render(<Socialgroup/>);
    const titleElem = screen.getByText("Social Groups");
    expect(titleElem).toBeInTheDocument();
  })

// Test 5
  test("Render NavBar page", () => {
    render(<NavBar/>);
    const titleElem = screen.getByText("Home");
    expect(titleElem).toBeInTheDocument();
  })

// Test 6
it("App initializes correctly", () => {
  const component = renderer.create(<App />);

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

describe('Page structures', () => {

  test('Gym Card', () => {
    const tree = <BrowserRouter>renderer.create(<GymCard />).toJSON()</BrowserRouter>
    expect(tree).toMatchSnapshot()
});

  test('Exercise Card', () => {
    const tree = <BrowserRouter>renderer.create(<ExerciseCard />).toJSON()</BrowserRouter>
    expect(tree).toMatchSnapshot()
});

  test('SocialGroup Card', () => {
    const tree = <BrowserRouter>renderer.create(<SocialGroupCard />).toJSON()</BrowserRouter>
    expect(tree).toMatchSnapshot()
});

  test('Navigation Bar', () => {
    const tree = <BrowserRouter>renderer.create(<NavBar />).toJSON()</BrowserRouter>
    expect(tree).toMatchSnapshot()
});

}

  
  
  

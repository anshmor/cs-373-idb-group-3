import json
from models import app, db, Gym, Exercise, Social_Group, City

# adapted from https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/back-end/db.py

city_id = 1
cities = {}

def populate_db():
    populate_gyms()
    populate_exercises()
    populate_social_groups()


def populate_gyms():
    with open("gyms_all.json", "r", encoding="utf-8") as f:
        file = json.load(f)
        count = 1
        for data in file:
            for gym in data["results"]:


                city = gym["vicinity"].split(", ")[-1]
                cur_city_id = 1
                if city in cities :
                    cur_city_id = cities[city]

                else :
                    global city_id
                    cities[city] = city_id
                    cur_city_id = city_id
                    city_id += 1
                    db.session.add(City(id = cur_city_id, city = city))

                db_row = {
                    "id": count,
                    "longitude": gym["geometry"]["location"]["lng"],
                    "latitude": gym["geometry"]["location"]["lat"],
                    "icon": gym["icon"],
                    "name": gym["name"],
                    "vicinity": gym["vicinity"],
                    "numberOfRatings": 0,
                    "rating": 0.0,
                    "operational": "UNKOWN",
                    "type1": "",
                    "type2": "",
                    "type3": "",
                    "city_id": cur_city_id 
                }

                if len(gym["types"]) >= 1:
                    db_row["type1"] = gym["types"][0]

                if len(gym["types"]) >= 2:
                    db_row["type2"] = gym["types"][1]

                if len(gym["types"]) >= 3:
                    db_row["type3"] = gym["types"][2]

                if "rating" in gym:
                    db_row["rating"] = float(gym["rating"])

                if "user_ratings_total" in gym:
                    db_row["numberOfRatings"] = int(gym["user_ratings_total"])

                if "business_status" in gym:
                    db_row["operational"] = gym["business_status"]

                db.session.add(Gym(**db_row))
                count += 1


def populate_exercises():
    with open("exercises_all.json", "r", encoding="utf-8") as f:
        file = json.load(f)
        count = 1
        for exercise in file["results"]:

            if (
                exercise["language"]["full_name"] == "English"
                and len(exercise["videos"]) != 0
                and len(exercise["muscles"]) != 0
            ):
                db_row = {
                    "id": count,
                    "name": exercise["name"],
                    "description": exercise["description"],
                    "category": exercise["category"]["name"],
                    "muscle_name": "",
                    "is_front": "",
                    "img": "",
                    "video": exercise["videos"][0]["video"],
                    "equipment": "",
                }

                if len(exercise["muscles"]) != 0:
                    db_row["muscle_name"] = exercise["muscles"][0]["name"]
                    db_row["is_front"] = exercise["muscles"][0]["is_front"]

                if len(exercise["images"]) != 0:
                    db_row["img"] = exercise["images"][0]["image"]

                if len(exercise["equipment"]) > 0:
                    db_row["equipment"] = exercise["equipment"][0]["name"]

                db.session.add(Exercise(**db_row))
                count += 1


def populate_social_groups():
    with open("social_groups_all.json", "r", encoding="utf-8") as f:
        file = json.load(f)
        count = 1
        for data in file:
            if "businesses" in data:

                for social_group in data["businesses"]:
                    
                    city = social_group["location"]["city"]
                    cur_city_id = 1
                    if city in cities :
                        cur_city_id = cities[city]

                    else :
                        global city_id
                        cities[city] = city_id
                        cur_city_id = city_id
                        city_id += 1
                        db.session.add(City(id = cur_city_id, city = city))

                    db_row = {
                        "id": count,
                        "longitude": social_group["coordinates"]["latitude"],
                        "latitude": social_group["coordinates"]["longitude"],
                        "address": social_group["location"]["address1"],
                        "city_id": cur_city_id,
                        "country": social_group["location"]["country"],
                        "state": social_group["location"]["state"],
                        "zip_code": social_group["location"]["zip_code"],
                        "name": social_group["name"],
                        "image": social_group["image_url"],
                        "rating": social_group["rating"],
                        "numberOfRatings": social_group["review_count"],
                        "url": social_group["url"],
                        "title": social_group["categories"][0][
                            "title"
                        ],  # multiple titles i could be storing, only taking first rn
                        "phone": social_group["display_phone"],
                    }

                    db.session.add(Social_Group(**db_row))
                    count += 1


if __name__ == "__main__":
    with app.app_context():
        db.session.remove()
        db.drop_all()
        db.create_all()
        populate_db()
        db.session.commit()

        city = "Aberdeen"
        social_groups = Social_Group.query.filter_by(city_id=city_id).all()

        for social_group in social_groups:
            print(Gym.query.filter_by(city_id=social_group['city_id']).all())

import Dropdown from 'react-bootstrap/Dropdown';
import { useState } from 'react';

//Dropdown Menu sorting options for social groups by highest and lowest ratings, and most and least rated.
//sortVal values match backend args for social group sorting.
//Referenced Geojobs and react-bootstrap while working on it
function ExerciseSort({selectSortValue, inputTitle}) {

const[menuTitle, setMenuTitle] = useState("Sort by")

/*    Add to Exercises.js, now takes selectSortValue, menuTitle props
  const[sortVal, setSortVal] = useState(null)
  const[menuTitle, setMenuTitle] = useState("Sort by")

   function selectSortValue(val){
        console.log(val)
        setSortVal(val)
        if (val === "Alphabetical"){
        setMenuTitle("Alphabetical")
        }
        if (val === "Alphabetical-Descending"){
        setMenuTitle("Alphabetical-Descending")
        }
    }
*/
  
  return (
      <Dropdown className="exercise-group-sort" >
        <Dropdown.Toggle variant="primary" id="dropdown-exercise-sort">
        {menuTitle}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          <Dropdown.Item id="alphabetical-asc-exercise-sort-selection" onClick={() => 
            {selectSortValue("Alphabetical")
            setMenuTitle("Alphabetical")}}>Alphabetical</Dropdown.Item>
          <Dropdown.Item id="alphabetical-desc-sort-selection" onClick={() => {
            selectSortValue("Alphabetical-Descending")
            setMenuTitle("Alphabetical-Descending")}}>Alphabetical-Descending</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
  );
}
export default ExerciseSort;

/*  Submit Button style
      <button className="sort-submit" style = {{backgroundColor: '#6c757d', 
      border: 'none', color: 'white', 'border-radius': '5px'}}>Submit</button>
*/

import React from 'react'
import { Card, Row, Col, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const homeInfo = [
  {
    name: "Gyms",
    info: "Explore more gyms!",
    image: './homePics/gymPic.jpg',
    link: '/gym'
  },
  {
    name: "Exercises",
    info: "Find the exercises that fit you the best!",
    image: './homePics/exercisePic.jpg',
    link: '/Exercises'
  },
  {
    name: "Social Groups",
    info: "Find your social group!",
    image: './homePics/sociagroupPic.jpg',
    link: '/SocialGroups'
  },
];

//     Nice light blue
//'rgb(217,231,247)'
//     Cream
//'rgb(251 248 234)'

export default function Home() {
  const bgColor = 'rgb(217,231,247)';
  const textStyle = {
    color: 'white',
    textAlign: 'center',
    paddingTop: '30vh',
    fontSize: '5rem'
  };
  
  const cardStyle = {
    width: '22rem',
    margin: "3rem",
    height: '32rem',
    // backgroundColor: bgColor,
    boxShadow: '5px 5px 5px gray'
  };
  
  const cardTextStyle = {
    minHeight: '7rem'
  };
  
  //global background color for site.
  document.body.style.backgroundColor = bgColor;

  return (
    <div className="bg d-flex justify-content-center"
      style={{
        backgroundImage: `url("/gymNewPic.png")`,
        backgroundSize: "cover",
        width: "100%",
        height: "75vh",
        position: "relative"
      }}
    >
      <div style={textStyle}>
      <h1 style={{ textShadow: '0 0 35px black, 0 0 35px black', fontSize: 60, fontWeight: 500, color: "#ffffff", textAlign: "center", marginBottom: 20 }}>Welcome to Flexercise</h1>        
      {/* <h1>Where you can find exercises, gyms, and social groups all at once!</h1> */}
      </div>
      <Container
        className="bottom"
        style={{
          backgroundColor: "white",
          textAlign: "center",
          paddingTop: "5vh",
          maxWidth: "100%",
          height: "1vh",
          position: "absolute",
          bottom: "0",
          left: "0",
          right: "0"
        }}
      >
        <Row style={{ display: "flex", justifyContent: "center" }}>
          {homeInfo.map((home) => {
            return (
              <Col md="auto" key={home.name}>
                <Card text={"dark"} style={cardStyle}>
                  <Card.Img
                    variant="top"
                    src={home.image}
                    width="200px"
                    height="250px"
                  />
                  <Card.Body>
                    <Card.Title>{home.name}</Card.Title>
                    <Card.Text style={cardTextStyle}>{home.info}</Card.Text>
                    <Link to={home.link}>
                      <Button variant="primary">More Info</Button>
                    </Link>
                  </Card.Body>
                </Card>
              </Col>
            );
          })}
        </Row>
      </Container>
    </div>
  );
}

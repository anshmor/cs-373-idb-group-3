import React, { useState, useEffect, useRef } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SocialGroupCard from './components/SocialGroupCard';
import axios from "axios";
import Pagination from "react-bootstrap/Pagination";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FilterDropdown from "./components/FilterDropdown";
import SocialGroupSort from "./components/SocialGroupSort";

export default function SocialGroupGrid() {
  const client = axios.create({
    baseURL: "https://api.flexercise.social/",
  });
  
  const searchQuery = useRef("");

  const [socialgroups, setSocialgroups] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [activePage, setActivePage] = useState(1);
  const [numTotal, setNumTotal] = useState(0);

  const [startRating, setStartRating] = useState(0.0);
  const [endRating, setEndRating] = useState(5.0);
  const [minNumRatings, setMinNumRatings] = useState(0);
  const [maxNumRatings, setMaxNumRatings] = useState(1000);
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [state, setState] = useState("");
  const [title, setTitle] = useState("");

  const[sortVal, setSortVal] = useState(null)
  const[menuTitle, setMenuTitle] = useState("Sort by")

  const[queryR, setQueryR] = useState(null);

  var queryRE = null;

  function selectSortValue(val){
    console.log(val)
    setSortVal(val)
    if (val === "By Highest Rating"){
    setMenuTitle("Highest rating")
    }
    if (val === "By Lowest Rating"){
    setMenuTitle("Lowest rating")
    }
    if (val === "By Highest Number of Ratings"){
    setMenuTitle("Most ratings")
    }
    if (val === "By Lowest Number of Ratings"){
        setMenuTitle("Least ratings")
    }
}

  
  function handleClick(number) {
    setActivePage(number);
    setLoaded(false);
  }

  function updateNumConstraints(category) {
    var val = document.getElementById(category).value;
    console.log(val);

    if (category === 'startRating') {
      if (val !== '0' && !isNaN(val)) {
        setStartRating(val);
      } else {
        setStartRating(0.0);
      }
    } else if (category === 'endRating') {
      if (val !== '0' && !isNaN(val)) {
        setEndRating(val);
      } else {
        setEndRating(5.0);
      }
    } else if (category === 'minNumRating') {
      if (val !== '0' && !isNaN(val)) {
        setMinNumRatings(val);
      } else {
        setMinNumRatings(0);
      }
    } else if (category === 'maxNumRating') {
      if (val !== '0' && !isNaN(val)) {
        setMaxNumRatings(val);
      } else {
        setMaxNumRatings(1000);
      }
    } else if (category === 'city') {
      if (val !== '') {
        setCity(val);
      } 
    } else if (category === 'country') {
      if (val !== '') {
        setCountry(val);
      } 
    } else if (category === 'state') {
      if (val !== '') {
        setState(val);
      } 
    }
  }
  // searching code inspo from https://gitlab.com/sarthaksirotiya/cs373-idb
  useEffect(() => {
    const fetchSocialgroups = async () => {
      if (!loaded) {
        var query = `social_groups?page=${activePage}&perPage=20`;
        const search = searchQuery.current.value;
        if (search) {
          query += `&search=${search}`;
          queryRE = new RegExp(
            `(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
            "i"
          );
          setQueryR(queryRE);
          console.log("queryRE: " + queryRE);
        }
        else if (sortVal) {
          query += `&sort=${sortVal}`;
          console.log(query)
        } 
        else {
          if (startRating != 0 || endRating != 5) {
            query += `&rating=${startRating},${endRating}`;
            console.log("adding rating filter query");
            console.log(query);
          }
          if (minNumRatings != 0 || maxNumRatings != 1000) {
            query += `&numberOfRatings=${minNumRatings},${maxNumRatings}`;
            console.log("adding num rating filter query");
            console.log(query);
          }
          if (city != "") {
            query += `&city=${city}`;
            console.log("adding city filter query");
            console.log(query);
          }
          if (country != "") {
            query += `&country=${country}`;
            console.log("adding country filter query");
            console.log(query);
          }
          if (state != "") {
            query += `&state=${state}`;
            console.log("adding state filter query");
            console.log(query);
          }
        }
        await client
          .get(query)
          .then((response) => {
            setSocialgroups(response.data);
            console.log(query);
            console.log(response.data);
          })
          .catch((err) => console.log(err));
        setLoaded(true);
      }
    };
    const getTotalNumSocialgroups = async () => {
      if (!loaded) {
        var query = `social_groupCount`;
        await client
          .get(query)
          .then((response) => {
            setNumTotal(response.data['count']);
            console.log(response.data);
          })
          .catch((err) => console.log(err));
      }
    };
    fetchSocialgroups();
    getTotalNumSocialgroups();
  }, [loaded]);

  useEffect(() => {
    console.log(socialgroups);
  }, [socialgroups])
  
  let numPages = Math.ceil(numTotal / 20);
  let items = [];
  for (let number = activePage - 2; number <= activePage + 2; number++) {
    if (number > 0 && number <= numPages) {
      items.push(
        <Pagination.Item
          key={number}
          onClick={() => handleClick(number)}
          active={number === activePage}
        >
          {number}
        </Pagination.Item>
      );
    }
  }

  return (
    <Container>
      <h1 id="social-group-title" className="d-flex justify-content-center p-5">Social Groups</h1>
      <Form
        onSubmit={(event) => {
          event.preventDefault();
          setLoaded(false);
        }}
        className="d-flex pb-5 justify-content-center"
      >
        <Form.Control
          id="social-group-search"
          ref={searchQuery}
          style={{ width: "20vw" }}
          type="search"
          placeholder="Search social groups"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-dark" onClick={() => setLoaded(false)}>
          Search
        </Button>
      </Form>
      {/* <div className=" pb-5 justify-content-center">
        <SocialGroupSort style={{ width: "20vw" }} menutitle={menuTitle} selectSortValue={selectSortValue}/>
        <h2 className="test field">{menuTitle}</h2>
        <Button id="social-group-sort-submit" variant="outline-dark" onClick={() => setLoaded(false)} >Submit</Button>
      </div> */}
      <Form className="filter-form">
        <Row className="mx-auto text-center w-50 my-4"> 
        <Col className=" pb-5 justify-content-center">
        <SocialGroupSort style={{ width: "20vw" }} menutitle={menuTitle} selectSortValue={selectSortValue}/>
        {/* <h2 className="test field">{menuTitle}</h2> */}
        <Button id="social-group-sort-submit" variant="outline-dark" onClick={() => setLoaded(false)} >Sort</Button>
      </Col>
          <Col>
            <div class = "form-group">
              <label for = "formGroupExampleInput">Min Rating(0.0 - 5.0)</label>
              <input 
                type="number"
                class="form-control"
                id="startRating"
                placeholder="0"
                onChange={() => updateNumConstraints("startRating")}
              ></input>
            </div>
            </Col>
            <Col>
            <div class = "form-group">
              <label for = "formGroupExampleInput">Max Rating(0.0 - 5.0)</label>
              <input 
              type="number"
              class="form-control"
              id="endRating"
              placeholder="5"
              onChange={() => updateNumConstraints("endRating")}
            ></input>
            </div>
          </Col>
          <Col>
            <div class = "form-group">
              <label for = "formGroupExampleInput">Min Num Ratings</label>
              <input 
                type="number"
                class="form-control"
                id="minNumRating"
                placeholder="0"
                onChange={() => updateNumConstraints("minNumRating")}
              ></input>
            </div>
            </Col>
            <Col>
            <div class = "form-group">
              <label for = "formGroupExampleInput">Max Num Ratings</label>
              <input 
              type="number"
              class="form-control"
              id="maxNumRating"
              placeholder="1000"
              onChange={() => updateNumConstraints("maxNumRating")}
            ></input>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
          <div class = "form-group">
              <label for = "formGroupExampleInput">City</label>
              <input 
              type="text"
              class="form-control"
              id="city"
              placeholder="city"
              onChange={() => updateNumConstraints("city")}
            ></input>
            </div>
          </Col>
          <Col>
          <div class = "form-group">
              <label for = "formGroupExampleInput">State</label>
              <input 
              type="text"
              class="form-control"
              id="state"
              placeholder="state"
              onChange={() => updateNumConstraints("state")}
            ></input>
            </div>
          </Col>
          <Col>
          <div class = "form-group">
              <label for = "formGroupExampleInput">Country</label>
              <input 
              type="text"
              class="form-control"
              id="country"
              placeholder="country"
              onChange={() => updateNumConstraints("country")}
            ></input>
            </div>
          </Col>

        </Row>
        <Row className="mx-auto text-center mt-4">
          <Col>
            <Button
              variant="outline-dark"
              onClick={() => setLoaded(false)}
            >
              Filter
            </Button>
          </Col>
        </Row> 
        
      </Form>

      <p style={{textAlign: "center"}}>Displaying 20 out of {numTotal} social group instances </p>
        <Row
          xl={5}
          lg={4}
          md={3}
          sm={2}
          xs={1}
          className="d-flex g-1 p-1"
          style={{marginRight: '15px', marginLeft: '15px'}}>
          {socialgroups.map((socialgroup) => {
              return (
                <Col className="d-flex align-self-stretch">
                  <SocialGroupCard socialgroup={socialgroup} regex={queryR}/>
                </Col>
              );
            })}
        </Row>
      <Pagination className="justify-content-center">
        {activePage > 3 && (
          <Pagination.Item
            first
            key={1}
            onClick={() => handleClick(1)}
            active={1 === activePage}
          >
          </Pagination.Item>
        )}
        {activePage > 4 && <Pagination.Ellipsis />}
        {items}
        {activePage < numPages - 3 && <Pagination.Ellipsis />}
        {activePage < numPages - 2 && (
          <Pagination.Item
            last
            key={numPages}
            onClick={() => handleClick(numPages)}
            active={numPages === activePage}
          >
            {numPages}
          </Pagination.Item>
        )}
      </Pagination>
    </Container>
  );
}

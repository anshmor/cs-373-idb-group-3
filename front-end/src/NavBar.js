import { Link } from 'react-router-dom';
// import '../../src/cssStyles/Navbar.css';
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";


function NavBar() {
  const handleSubmit = (event) => {
    event.preventDefault()
    const form = event.currentTarget
    console.log(`search query: ${form.query.value}`)
    window.location.assign(`/search/${form.query.value}`)
  }
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{ position: 'fixed', top: 0, left: 0, width: '100%', zIndex: 1 }}>
      <Link className="navbar-brand" to="/">
        <img src="muscleLogo.svg" alt="logo" className="navbar-brand" style={{ width: "60px", height: "60px" }}></img>
      </Link>
      <Link className="navbar-brand" to="/" style={{fontSize: "20px"}}>Flexercise</Link>
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link className="nav-link" to="/" style={{fontSize: "20px"}}>Home</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/about" style={{fontSize: "20px"}}>About</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/gym" style={{fontSize: "20px"}}>Gyms</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/socialGroups" style={{fontSize: "20px"}}>Social Groups</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/exercises" style={{fontSize: "20px"}}>Exercises</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/visualizations" style={{fontSize: "20px"}}>Visualizations</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/providerVisualizations" style={{fontSize: "20px"}}>Provider Visualizations</Link>
        </li>
        <Form onSubmit={handleSubmit} className="d-flex">
              <Form.Control
                style={{ width: "20vw" }}
                type="search"
                name="query"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
              />
              <Button type="submit" variant="outline-secondary" style={{color: '#3d405b'}}>Search</Button>
        </Form>
      </ul>
    </nav>
  );
}

export default NavBar;

import React from "react";

const MapEmbed = (props) => {
  const {
    name,
    addr
  } = props.map

  let map_search = name.split(" ").join('+') + "+" + addr.split(" ").join('+')

  map_search = map_search.split("&").join('+')

  console.log(map_search);

  return (
    <div className="map">
    <iframe 
    width="200" 
    height="200" loading="lazy"
    src={map_search && "https://www.google.com/maps/embed/v1/place?q=" + map_search + "&key=AIzaSyDBv1F_z-Q7pu7ibj8FgDFkkdE58nuU_6o"}></iframe>
  </div>
  )
};

export default MapEmbed;
import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import {Highlight} from "react-highlight-regex";


function OText({ children }) {
  return (
    <span style={{  fontSize: '17px', color: 'white', font: 'Courier-Oblique'  }}>{children}</span>
  );
}

const SocialGroupCard = (props) => {
  const {
    name,
    address,
    city,
    state,
    country,
    image,
    latitude,
    longitude,
    title: type,
    numberOfRatings: review_count,
    rating,
    phone,
    url,
    id,
  } = props.socialgroup;

  function formatType() {
    return type[0].title;
  }

  function highlightText(input) {
    
    if (props.regex != null && input != null) {
      console.log("props.regex: " + props.regex);
      console.log("run highlight funct")
      console.log("this should be highlighted");
      return <Highlight match={props.regex} text={input} />;
    }
    return input;
  }

  const googlemapapikey = "AIzaSyBJtCjUkBi3rNISAfn5xAKnG7QpGq3hTtk"

  function initMap() {
    var url = "https://www.google.com/maps/embed/v1/search?key=YAIzaSyBJtCjUkBi3rNISAfn5xAKnG7QpGq3hTtk&center=" + latitude + longitude;
    return <iframe
      width="450"
      height="250"
      frameborder="0" style="border:0"
      referrerpolicy="no-referrer-when-downgrade"
      src={url}
      allowfullscreen>
    </iframe>
  }

  return (
    <Card
    style={{ height: '30rem'}}>
      <Card.Img
        variant="top"
        src={image}
        style={{ height: '40%', width: '100%', objectFit: 'cover' }}
      ></Card.Img>
      <Card.Body style={{height: '30%'}}>
        <Card.Title>{highlightText(name)}</Card.Title>
        <Card.Subtitle>{highlightText("Address: " + address + ", " + city + "," + state + ", " + country)}</Card.Subtitle>
        <Card.Text  name="social-group-text">
          {highlightText("Type: " + type)}<br></br>
          {highlightText("Rating: " + rating)}<br></br>
          {highlightText("Number of Reviews: " + review_count)}<br></br>
          {highlightText("Phone Number:" + phone)}
          </Card.Text>
      </Card.Body>
      <Card.Footer
      style= {{backgroundColor: '#3d405b'}}>
        <Button
          name = "social-group-instance-button"
          style= {{backgroundColor: '#e07a5f'}}
          className="btn btn-primary stretched-link"
          variant="dark"
          href={`/socialGroup/${id}`}
        >
          <OText>More Info</OText>
        </Button>
      </Card.Footer>
    </Card>
  );
};

export default SocialGroupCard;
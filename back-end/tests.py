import app      
import unittest 
import json

# adapted from https://gitlab.com/sriyab/workitout/-/blob/main/backend/tests.py

class testEndpoints(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()


    def test_home(self):
        with self.client:
            response = self.client.get("/")
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)

    def test_exercises_1(self):
        with self.client:
            response = self.client.get("exercises?page=1&perPage=20")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json), 20)

    def test_exercises_2(self):
        with self.client:
            response = self.client.get("exercises?page=1&perPage=20")
            response2 = self.client.get("exercises?page=2&perPage=20")
            self.assertEqual(response2.status_code, 200)
            res_json = json.loads(response.data)
            res_json2 = json.loads(response2.data)
            self.assertNotEqual(res_json, res_json2)

    def test_exercises_3(self):
        with self.client:
            response = self.client.get("exercises")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreaterEqual(len(res_json), 40)

    def test_exerciseById_1(self):
        with self.client:
            response = self.client.get("exerciseById?id=1")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json['category'], 'Legs')

    def test_exerciseById_2(self):
        with self.client:
            response = self.client.get("exerciseById")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json), 0)


    def test_gyms_1(self):
        with self.client:
            response = self.client.get("gyms?page=1&perPage=20")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json), 20)

    def test_gyms_2(self):
        with self.client:
            response = self.client.get("gyms?page=1&perPage=20")
            response2 = self.client.get("gyms?page=2&perPage=20")
            self.assertEqual(response2.status_code, 200)
            res_json = json.loads(response.data)
            res_json2 = json.loads(response2.data)
            self.assertNotEqual(res_json, res_json2)

    def test_gyms_3(self):
        with self.client:
            response = self.client.get("gyms?page=1&perPage=1000")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreaterEqual(len(res_json), 500)

    def test_gymById_1(self):
        with self.client:
            response = self.client.get("gymById?id=1")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json['city'], 'Aberdeen')

    def test_gymById_2(self):
        with self.client:
            response = self.client.get("gymById")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json), 0)


    def test_social_groups_1(self):
        with self.client:
            response = self.client.get("social_groups?page=1&perPage=20")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json), 20)

    def test_social_groups_2(self):
        with self.client:
            response = self.client.get("social_groups?page=1&perPage=20")
            response2 = self.client.get("social_groups?page=2&perPage=20")
            self.assertEqual(response2.status_code, 200)
            res_json = json.loads(response.data)
            res_json2 = json.loads(response2.data)
            self.assertNotEqual(res_json, res_json2)

    def test_social_groups_3(self):
        with self.client:
            response = self.client.get("social_groups?page=1&perPage=2000")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreaterEqual(len(res_json), 1000)

    def test_social_groupById_1(self):
        with self.client:
            response = self.client.get("social_groupById?id=1")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json['city'], 'Aberdeen')

    def test_social_groupById_2(self):
        with self.client:
            response = self.client.get("social_groupById")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json), 0)

    def test_exerciseCount(self):
        with self.client:
            response = self.client.get("exerciseCount")
            res_json = json.loads(response.data)
            self.assertGreaterEqual(res_json['count'], 20)

    def test_social_groupCount(self):
        with self.client:
            response = self.client.get("social_groupCount")
            res_json = json.loads(response.data)
            self.assertGreaterEqual(res_json['count'], 100)

    def test_gymCount(self):
        with self.client:
            response = self.client.get("gymCount")
            res_json = json.loads(response.data)
            self.assertGreaterEqual(res_json['count'], 100)


if __name__ == "__main__":  # pragma: no cover
    unittest.main()

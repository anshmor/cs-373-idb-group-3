from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import ARRAY
from flask_cors import CORS

# adapted from https://gitlab.com/sarthaksirotiya/cs373-idb/-/tree/main/back-end

app = Flask(__name__)
CORS(app)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://newuser:password@localhost:5432/postgres'
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://postgres:group3swe@swe.c0qmrcffy2zg.us-east-2.rds.amazonaws.com:5432/swe"
db = SQLAlchemy(app)

# gym_social_group = db.Table('gym_social_group',
#     db.Column('gym_id', db.Integer, db.ForeignKey('gyms.id')),
#     db.Column('social_group_id', db.Integer, db.ForeignKey('social_groups.id'))
# )

class City(db.Model):
    __tablename__ = 'city'

    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(80))

class Gym(db.Model):
    __tablename__ = "gym"

    id = db.Column(db.Integer(), primary_key=True)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    icon = db.Column(db.String(150))
    name = db.Column(db.String(120))
    rating = db.Column(db.Float)
    numberOfRatings = db.Column(db.Integer())
    vicinity = db.Column(db.String(150))  # streets and places near
    #city = db.Column(db.String(50))
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'))
    operational = db.Column(db.String(20))
    type1 = db.Column(
        db.String(40)
    )  # what services available here, ex. lodging, gym, restaurant, bar
    type2 = db.Column(db.String(40))
    type3 = db.Column(db.String(40))

    #social_groups = db.relationship('Social_Group', backref='gyms')

    def to_dict(self):
        return {
            "id": self.id,
            "longitude": self.longitude,
            "latitude": self.latitude,
            "icon": self.icon,
            "name": self.name,
            "rating": self.rating,
            "numberOfRatings": self.numberOfRatings,
            "vicinity": self.vicinity,
            "city": City.query.filter_by(id=self.city_id).first().city,
            "operational": self.operational,
            "type1": self.type1,
            "type2": self.type2,
            "type3": self.type3,
        }

class Exercise(db.Model):
    __tablename__ = "exercise"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    description = db.Column(db.String())
    category = db.Column(db.String(50))
    equipment = db.Column(db.String(50))
    muscle_name = db.Column(db.String(50))
    is_front = db.Column(db.Boolean)
    img = db.Column(db.String(200))
    video = db.Column(db.String(200))

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "category": self.category,
            "equipment": self.equipment,
            "muscle_name": self.muscle_name,
            "is_front": self.is_front,
            "img": self.img,
            "video": self.video,
        }


class Social_Group(db.Model):
    __tablename__ = "social_group"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    image = db.Column(db.String(250))
    url = db.Column(db.String(250))
    numberOfRatings = db.Column(db.Integer)
    rating = db.Column(db.Float)
    address = db.Column(db.String(80))
    #city = db.Column(db.String(80))
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'))
    country = db.Column(db.String(50))
    state = db.Column(db.String(50))
    zip_code = db.Column(db.String(10))
    title = db.Column(db.String(80))
    phone = db.Column(db.String(35))
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)

    #gyms = db.relationship('Gym', backref='social_groups')

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "image": self.image,
            "url": self.url,
            "numberOfRatings": self.numberOfRatings,
            "rating": self.rating,
            "address": self.address,
            "city": City.query.filter_by(id=self.city_id).first().city,
            "country": self.country,
            "state": self.state,
            "zip_code": self.zip_code,
            "title": self.title,
            "phone": self.phone,
            "longitude": self.longitude,
            "latitude": self.latitude,
        }


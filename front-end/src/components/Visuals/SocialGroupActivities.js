import React, { useState, useEffect } from "react";
import { PieChart, Pie, Legend, Tooltip, ResponsiveContainer, Cell, Label } from 'recharts';
import axios from "axios";

function SocialGroupActivities() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const COLORS = ["#CB3503","#E65D0A","#FB931F","#372B4F", "#8070c0"]


  const client = axios.create({
      baseURL: "https://api.flexercise.social/",
    });

  useEffect(() => {
    const fetchSocialGroups = async () => {
    
        var query = `social_groups?page=1&perPage=3000`;
        var social_groups;
        setLoading(true);
        await client
            .get(query)
            .then((response) => {
                social_groups = response.data;
                
            })
            .catch((err) => console.log(err))
            .finally(() => {
                setLoading(false);
            })

        // Build Map with muscle group to frequency of exercises with that muscle group
        const socialGroupCategories = new Map();
        for (var i = 0; i < social_groups.length; i++) {
            if (socialGroupCategories.has(social_groups[i]['title'])) {
                socialGroupCategories.set(social_groups[i]['title'], socialGroupCategories.get(social_groups[i]['title']) + 1);
            }
            else {
                socialGroupCategories.set(social_groups[i]['title'], 1);
            }
        }
        
        // convert map into data for pie chart
        var tempData = [];
        socialGroupCategories.forEach((value, key, map) => {
            if (value > 50) {
                tempData.push({'activity': key, 'count': value});
            }
        })
        
        setData(tempData);
    };

    fetchSocialGroups();
  }, []);


  return (
   
    <div>
    <h1 style={{display: "flex", justifyContent: "center"}}>Social Groups by Popular Activities</h1>
    {loading && <h2 style={{display: "flex", justifyContent: "center"}}>Loading...</h2>}
    <PieChart width={730} height={500}>
        <Pie data={data} dataKey="count" nameKey="activity" 
            cx="50%" cy="50%" fill= "#FFC300" label>
            {
        data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
        }
        </Pie>
        <Tooltip />
    </PieChart>
</div>
    
  );
}

export default SocialGroupActivities;

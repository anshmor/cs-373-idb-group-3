#  code inspired by WorkItOut and https://nander.cc/using-selenium-within-a-docker-container
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import unittest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.common.keys import Keys

PATH = "./__tests__/chromedriver"
url = "https://www.flexercise.social/"
#url = "http://localhost:3000/"



class GuiTests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        self.driver = webdriver.Chrome(PATH, options=chrome_options)
        
    def tearDown(self):
        self.driver.quit()

    
    # Test 1
    def testTitle(self):
        self.driver.get(url)
        self.assertEqual(self.driver.title, "Flexercise")


    # Test 2 
    def testAbout(self):
        self.driver.get(url + 'about')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "What is Flexercise")


    # Test 3
    def testGyms(self):
        self.driver.get(url + 'gym')
        self.assertEqual(self.driver.find_element(By.ID, "gym-title").text, "Gyms")

    

    # Test 4
    def testExercises(self):
        self.driver.get(url + 'exercises')
        self.assertEqual(self.driver.find_element(By.ID, "exercise-title").text, "Exercises")


    # Test 5
    def testSocialGroups(self):
        self.driver.get(url + 'socialGroups')
        self.assertEqual(self.driver.find_element(By.ID, "social-group-title").text, "Social Groups")


    # Test 6
    def testNav(self):
        self.driver.get(url + 'about')

        self.assertEqual(
            self.driver.find_elements(By.CLASS_NAME, "nav-link")[0].get_attribute('href'),
            url
        )

    # Test 7
    def testNav2(self):
        self.driver.get(url)
        self.assertEqual(
            self.driver.find_elements(By.CLASS_NAME, "nav-link")[2].get_attribute('href'),
            url + 'gym'
        )

    # Test 8
    def testGymInstance(self):
        self.driver.get(url + 'gym')
        time.sleep(3)
        btn = self.driver.find_element(By.NAME, "gym-instance-button")

        tgt = btn.get_attribute('href')
        self.driver.execute_script("arguments[0].click();", btn)
        self.assertEqual(self.driver.current_url, tgt)




    # Test 9
    def testExerciseInstance(self):
        self.driver.get(url + 'exercises')
        time.sleep(3)
        btn = self.driver.find_element(By.NAME, "exercise-instance-button")
        tgt = btn.get_attribute('href')
        self.driver.execute_script("arguments[0].click();", btn)
        self.assertEqual(self.driver.current_url, tgt)




    # Test 10

    def testSocialInstance(self):
        self.driver.get(url + 'socialGroups')
        time.sleep(5)
        btn = self.driver.find_element(By.NAME, "social-group-instance-button")
        tgt = btn.get_attribute('href')
        self.driver.execute_script("arguments[0].click();", btn)
        self.assertEqual(self.driver.current_url, tgt)

    # Test 11 sorting gyms by highest rating

    def testGymSortByHighest(self):
        self.driver.get(url + 'gym')
        time.sleep(5)
        btn = self.driver.find_element(By.ID,"dropdown-gym-sort")
        self.driver.execute_script("arguments[0].click();", btn)
        self.driver.find_element(By.ID,"highest-rating-gym-sort-selection").click()
        self.driver.find_element(By.ID,"gym-sort-submit").click()
        time.sleep(5)
        rating = self.driver.find_element(By.NAME, "rating").text
        self.assertEqual(rating,'Rating: 5')


    # Test 12 sorting gyms by lowest rating

    def testGymSortByLowest(self):
        self.driver.get(url + 'gym')
        time.sleep(5)
        btn = self.driver.find_element(By.ID,"dropdown-gym-sort")
        self.driver.execute_script("arguments[0].click();", btn)
        self.driver.find_element(By.ID,"lowest-rating-gym-sort-selection").click()
        self.driver.find_element(By.ID,"gym-sort-submit").click()
        time.sleep(5)
        rating = self.driver.find_element(By.NAME, "rating").text
        self.assertEqual(rating,'Rating: 0')

    # Test 13 sorting social groups by lowest rating

    def testSocialgroupSortByLowest(self):
        self.driver.get(url + 'socialGroups')
        time.sleep(5)
        btn = self.driver.find_element(By.ID,"dropdown-social-group-sort")
        self.driver.execute_script("arguments[0].click();", btn)
        self.driver.find_element(By.ID,"lowest-rating-social-group-sort-selection").click()
        self.driver.find_element(By.ID,"social-group-sort-submit").click()
        time.sleep(5)
        result_text = self.driver.find_element(By.NAME, "social-group-text").text
        print(result_text)
        rating_test = "Rating: 1" in result_text
        self.assertTrue(rating_test)

    # Test 14 sorting social groups by highest rating

    def testSocialgroupSortByHighest(self):
        self.driver.get(url + 'socialGroups')
        time.sleep(5)
        btn = self.driver.find_element(By.ID,"dropdown-social-group-sort")
        self.driver.execute_script("arguments[0].click();", btn)
        self.driver.find_element(By.ID,"highest-rating-social-group-sort-selection").click()
        self.driver.find_element(By.ID,"social-group-sort-submit").click()
        time.sleep(5)
        result_text = self.driver.find_element(By.NAME, "social-group-text").text
        rating_test = "Rating: 5" in result_text
        self.assertTrue(rating_test)

    # Test 15 search gyms

    def testGymSearch(self):
        self.driver.get(url + 'gym')
        time.sleep(3)
        search = self.driver.find_element(By.ID,"gym-search")
        search.send_keys("sport")
        search.send_keys(Keys.RETURN)
        time.sleep(5)
        result_text = self.driver.find_element(By.NAME, "gym-card-name").text
        search_test = "sport" in result_text
        self.assertTrue(search_test)


   # Test 16 search gyms

    def testSocialGroupSearch(self):
        self.driver.get(url + 'socialGroups')
        time.sleep(3)
        search = self.driver.find_element(By.ID,"social-group-search")
        search.send_keys("sport")
        search.send_keys(Keys.RETURN)
        time.sleep(5)
        result_text = self.driver.find_element(By.NAME, "social-group-card-name").text
        search_test = "sport" in result_text
        #print(result_text)
        self.assertTrue(search_test)

   # Test 17 search exercises

    def testExerciseSearch(self):
        self.driver.get(url + 'exercises')
        time.sleep(3)
        search = self.driver.find_element(By.ID,"exercise-search")
        search.send_keys("curl")
        search.send_keys(Keys.RETURN)
        time.sleep(5)
        result_text = self.driver.find_element(By.NAME, "exercise-card-name").text
        search_test = "curl" in result_text.lower()
        print(result_text)
        self.assertTrue(search_test)

   # Test 18 filter exercise

    def testExerciseFilter(self):
        self.driver.get(url + 'exercises')
        time.sleep(3)
        div = self.driver.find_element(By.NAME,"muscle-name-filter")
        btn = div.find_element(By.TAG_NAME,"div").find_element(By.TAG_NAME,"button")
        self.driver.execute_script("arguments[0].click();", btn)
        self.driver.find_element(By.LINK_TEXT,"Trapezius").click()
        self.driver.find_element(By.ID,"exercise-filter-submit").click()  
        time.sleep(5)      
        result_text = self.driver.find_element(By.NAME, "exercise-card-text").text
        rating_test = "Muscle: Trapezius" in result_text
        self.assertTrue(rating_test)   

   # Test 19 filter gyms

    def testGymFilter(self):
        self.driver.get(url + 'gym')
        time.sleep(3)
        div = self.driver.find_element(By.NAME,"gym-operational-filter")
        btn = div.find_element(By.TAG_NAME,"div").find_element(By.TAG_NAME,"button")
        self.driver.execute_script("arguments[0].click();", btn)
        self.driver.find_element(By.LINK_TEXT,"CLOSED_TEMPORARILY").click()
        self.driver.find_element(By.ID,"gym-filter-submit").click()  
        time.sleep(5)      
        result_text = self.driver.find_element(By.NAME, "operational-text").text
        rating_test = "Not operational" in result_text
        self.assertTrue(rating_test)      


if __name__ == "__main__":
    unittest.main()

    







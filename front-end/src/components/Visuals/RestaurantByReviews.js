import React, { useState, useEffect, Container, Row, Col } from "react";

import axios from "axios";

import {  Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';

function RestaurantByReviews() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const client = axios.create({
      baseURL: "https://api.cityscoop.me/",
    });

  useEffect(() => {
    const fetchRestaurants = async () => {
    
        var query = 'restaurants';
        var restaurants
        setLoading(true);
        await client
            .get(query)
            .then((response) => {
                restaurants = response.data['Result'];
            })
            .catch((err) => console.log(err))
            .finally(() => {
                setLoading(false);
            })

        // Build Map with muscle group to frequency of exercises with that muscle group
        const ratings = new Map();
        ratings.set(0, 0);
        ratings.set(1, 0);
        ratings.set(2, 0);
        ratings.set(3, 0);
        ratings.set(4, 0);
        ratings.set(5, 0);
        for (var i = 0; i < restaurants.length; i++) {
            let rating = Math.floor(restaurants[i]['rest_rating']);
            if (ratings.has(rating)) {
                ratings.set(rating, ratings.get(rating) + 1);
            }
        }
        
        // convert map into data for pie chart
        var tempData = [];
        ratings.forEach((value, key, map) => {
            tempData.push({'rating': key, 'count': value});
        })
        setData(tempData);   
    };

    fetchRestaurants();
  }, []);


  return (
    <div>
        <h1 style={{display: "flex", justifyContent: "center"}}>Restaurant Ratings</h1>
        {loading && <h2 style={{display: "flex", justifyContent: "center"}}>Loading...</h2>}
        <RadarChart cx={300} cy={250} outerRadius={200} width={600} height={500} data={data}>
            <PolarGrid />
            <PolarAngleAxis dataKey="rating" />
            <PolarRadiusAxis />
            <Radar name="count" dataKey="count" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
        </RadarChart>

    </div>
  );
}

export default RestaurantByReviews;

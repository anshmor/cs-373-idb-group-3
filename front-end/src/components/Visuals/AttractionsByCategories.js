import React, { useState, useEffect } from "react";
import { PieChart, Pie, Legend, Tooltip, ResponsiveContainer, Cell, Label } from 'recharts';
import axios from "axios";

const AttractionsByCategories = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);

    const COLORS = ["#CB3503","#E65D0A","#FB931F","#372B4F", "#8070c0"]

    const client = axios.create({
        baseURL: "https://api.cityscoop.me/",
      });

    useEffect(() => {
        const fetchAttractions = async () => {
        
            var query = `attractions`;
            var attractions;
            setLoading(true);
            await client
                .get(query)
                .then((response) => {
                    attractions = response.data['Result'];
                })
                .catch((err) => console.log(err))
                .finally(() => {
                    setLoading(false);
                });

            // Build Map with muscle group to frequency of exercises with that muscle group
            const attractionCategories = new Map();
            for (var i = 0; i < attractions.length; i++) {
                for (var j = 0; j < attractions[i]['att_categories'].length; j++) {
                    if (!attractions[i]['att_categories'][j].includes('.')) {
                        if (attractionCategories.has(attractions[i]['att_categories'][j])) {
                            attractionCategories.set(attractions[i]['att_categories'][j], attractionCategories.get(attractions[i]['att_categories'][j]) + 1);
                        }
                        else {
                            attractionCategories.set(attractions[i]['att_categories'][j], 1);
                        }
                    }
                }
            }
            
            // convert map into data for pie chart
            var tempData = [];
            attractionCategories.forEach((value, key, map) => {
                tempData.push({'name': key, 'value': value});
            })
            
            setData(tempData);
        };

    fetchAttractions();
    }, []);

    return ( 
    <div>
        <h1 style={{display: "flex", justifyContent: "center"}}>Attractions By Categories</h1>
        {loading && <h2 style={{display: "flex", justifyContent: "center"}}>Loading...</h2>}
        <PieChart   width={730} height={500}>
            <Pie data={data} dataKey="value" nameKey="name" 
              cx="50%" cy="50%" fill= "#FFC300" label>
                {
          	data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
          }
            </Pie>
               <Tooltip />
        </PieChart>
    </div>
        );
}
 
export default AttractionsByCategories;